# readtext.py
#
# read the text file and do analysis
#
# April 2017, LKS
#
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd

#
#
fname='TestResults.txt'
counter=0
Names=['showName', 'showDiff', 'showStd', 'ShowType', 'ShowTypeStd', 'Overlap']
df={'showName':[], 'showDiff':[], 'showStd':[], 'ShowType':[], 'ShowTypeStd':[], 'Overlap':[]}
with open(fname) as f:
    content = f.readlines()
    
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]
for item in content:
    temp=item.split(':')
    if counter == 0:
        df[Names[counter]].append(temp[1])
    else:
        df[Names[counter]].append(float(temp[1]))

    counter+=1
    if counter>5:
        counter=0

#
# now compare
namesList=[]
for i in range(len(df['showName'])):
    if df['showDiff'][i] > df['ShowType'][i]:
        print(df['showName'][i])
        print(df['showDiff'][i])
        print(df['ShowType'][i])
        namesList.append(df['showName'][i])
