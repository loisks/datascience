# testCaseScorpion.py
#
# test out if profiling the shows by ad viewability works or not
#
# LKS, March 2017
#
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
import pickle
import json
import string
from sqlalchemy import create_engine

def create_db_engine(db_type, host, port, database, user, password):
    return create_engine(db_type + '://' + user + ':' + password + '@' + host + ':' + port + '/' + database)


def create_db_engine_with_credentials(db_cred_file_name, conn_name):
    with open(db_cred_file_name) as json_file:
        db_credentials = json.load(json_file)
        conn = db_credentials[conn_name]
        # print type(conn['port'])
        return create_db_engine(conn['db_type'], conn['host'], conn['port'], conn['database'],
                                conn['user'], conn['password'])
redshift_engine = create_db_engine_with_credentials('/audience-core/credentials/db_credentials.json', 'ispot_redshift')
mysql_engine = create_db_engine_with_credentials('/audience-core/credentials/db_credentials.json', 'ispot_db_mysql')

def apostropheFix(arrayOfNames):
       arrayOfNames=arrayOfNames.replace("'s" ,"''s")
       arrayOfNames=arrayOfNames.replace("O'N", "O''N")
       arrayOfNames=arrayOfNames.replace("'70s", "''70s")
       return(arrayOfNames)

def dataFetch(date1, date2, tt):
    query=\
           """
           SELECT adid, AVG(
	   CASE WHEN duration_sec < 16 AND percent_viewed <80 THEN percent_viewed+20
		WHEN duration_sec > 16 AND percent_viewed <85 THEN percent_viewed+15
		ELSE percent_viewed END), STDDEV(
	   CASE WHEN duration_sec < 16 AND percent_viewed <80 THEN percent_viewed+20
		WHEN duration_sec > 16 AND percent_viewed <85 THEN percent_viewed+15
	        ELSE percent_viewed END
           ),
           duration_sec
           FROM ap_results_production_output_inferencing_30_day_view inf
           JOIN ispot_db.adoccurence ado on occurrence_id = ado.occurenceid
           JOIN ispot_db.schedules sched on ado.scheduleid = sched.scheduleid
           JOIN ispot_db.programs series on sched.program = series.tmsid
           JOIN ispot_db.ads_subset addu on ado.adid = addu.ad_id
           WHERE series.title in ({2})
           AND est_occurencedatetime BETWEEN '{0} 00:00:00.000000' AND '{1} 00:00:00.000000'
          GROUP BY adid, duration_sec
HAVING AVG(
	CASE WHEN duration_sec < 16 AND percent_viewed <80 THEN percent_viewed+20
		WHEN duration_sec > 16 AND percent_viewed <85 THEN percent_viewed+15
		ELSE percent_viewed END
) is not null;

            """.format(date1, date2, tt)
         
    return( pd.read_sql_query(query, con=redshift_engine, coerce_float=False)  )  


# to automate you need to have it automatically do the queries and pull the data
# first one that needs to happen is one to get the show type

# first get the list of shows
#SimilarShows=pickle.load(open('SimilarShows.p', 'rb'))
# the first column is the index
#iterable=len(SimilarShows)
#names=[]
#SimilarShowData=[]
showName='The Big Bang Theory'
myfile = open('TestResults2.txt', 'wb')
#for item in range(iterable):
#    names.append(SimilarShows[item][0])
#    SimilarShowData.append(SimilarShows[item][1:])
# ok now do quick queries to get the showType
showTypes=[]
#for item in range(iterable):
#    showName=names[item]
query = \
            """
         select sc2.name
            FROM ispot_db.seriescategory sc 
            join ispot_db.series s on sc.seriesid = s.seriesid
            JOIN ispot_db.showcategories sc2 on sc2.showcategoryid = sc.showcategoryid
            WHERE s.title = "{0}"
            LIMIT 1
            """.format(showName)
showType = pd.read_sql_query(query, con=mysql_engine, coerce_float=False)
try:
        showTypes.append(showType.values[0][0])
except:
        showTypes.append([])

#
#
# now enter appropriate query for getting the modified show type data
date1='2017-03-01'
date2='2017-03-15'

dateTest1='2017-03-15'
dateTest2='2017-03-22'
for item2 in range(iterable):
    tt=','.join(map("'{0}'".format, list(SimilarShows[item2])))
    tt=apostropheFix(tt)
    
    # now we have the similar show data
    # get the show Type data now
    # first have to get a query with the show type data 
    #
    try:
       result=dataFetch(date1, date2, tt)
       queryShows=\
                """
                SELECT s.title series_title
             
                FROM ispot_data.master_dsd mdsd 
                JOIN ispot_db.schedules sch on sch.scheduleid=mdsd.scheduleid
                JOIN ispot_db.programs p on sch.program=p.id
                JOIN ispot_db.series s on s.seriesid=p.series
                JOIN ispot_db.seriescategory sc1 on s.seriesid = sc1.seriesid
                JOIN ispot_db.showcategories sc on sc1.showcategoryid = sc.showcategoryid
                WHERE sch.show_reach = 'N'
                AND mdsd.channelid in (105, 104, 107, 113)
                AND sc.name = '{2}'
                AND mdsd.date_est BETWEEN '{0}' AND '{1}'
                """.format(date1, date2, showTypes[item2])
       temp = pd.read_sql_query(queryShows, con=mysql_engine, coerce_float=False)
       seriesList=temp.series_title.unique()
       #
       # now we got the similar shows, quantify how many match between show type and similar shows
       matches=set(SimilarShows[item2]) & set(seriesList)
       percentMatch=200.0*len(matches)/(len(tt)+len(seriesList))
       print('Percent Match for '+ names[item2] + ' is: '+str(int(100*percentMatch)/100.))
    
       dd=','.join(map("'{0}'".format, list(seriesList)))
       dd=apostropheFix(dd)
       resultShowTypes=dataFetch(date1, date2, dd)
       tname=names[item2]
       tname=apostropheFix(tname)      
       #resultShow=dataFetch(date1, date2,"'{0}'".format(tname))
       resultTestShow=dataFetch(dateTest1, dateTest2, "'{0}'".format(tname))

       # great, now run the analysis part
       dfs=[resultTestShow, result , resultShowTypes]
       df_final = reduce(lambda left,right: pd.merge(left,right,on='adid'), dfs)
    
       diff=np.abs(np.array(df_final['avg_x'])-np.array(df_final['avg_y']))# select
       diff2=np.abs(np.array(df_final['avg_x'])-np.array(df_final['avg'])) # show Type

       print('Show is: ' + str(names[item2]) +'\n')
       print('Similar show diff is: '+ str(int(100*float(np.nanmean(diff)))/100.) +'\n')
       print('Show Type diff is: '+ str(int(100*float(np.nanmean(diff2)))/100.) +'\n')

       # write to a file
       myfile.write('Show is: ' + str(names[item2]) +'\n')
       myfile.write('Similar show diff is: '+ str(int(100*float(np.nanmean(diff)))/100.) +'\n')
       myfile.write('Similar show std is: '+ str(int(100*float(np.array(df_final['avg_y']).std()))/100.) +'\n')
       myfile.write('Show Type diff is: '+ str(int(100*float(np.nanmean(diff2)))/100.) +'\n')
       myfile.write('Show Type std is: '+ str(int(100*float(np.array(df_final['avg']).std()))/100.) +'\n')
       myfile.write('Percentage Match is: '+str(int(100*percentMatch)/100.) +'\n')

    except:
        print('some kind of error')


    
myfile.close()


    
    

# first load in the csv
#dataScorp=pd.read_csv('ChicagoMedResults.csv', delimiter=';', header=0)
#dataShowType=pd.read_csv('ChicagoMedShowTypes.csv', delimiter=';', header=0)
#dataSelect=pd.read_csv('ChicagoMedSimilarShows.csv', delimiter=';', header=0)
#
#
#CorrectAds=dataScorp.adid.unique()
#
#
## merge
##df=pd.concat([dataScorp, dataShowType, dataSelect], axis=1,keys=['adid'], join='inner', on)
#dfs=[dataScorp, dataSelect, dataShowType]
##df=dataScorp.merge(dataShowType,on='adid').merge(dataSelect,on='adid')
#df_final = reduce(lambda left,right: pd.merge(left,right,on='adid'), dfs)
#cc=pd.merge(dataScorp, dataSelect, on='adid', how='inner')
#dd=pd.merge(cc, dataShowType, on='adid', how='inner')
#
#diff=np.array(dd['avg_x'])-np.array(dd['avg_y'])# select
#diff2=np.array(dd['avg_x'])-np.array(dd['avg']) # show Type
