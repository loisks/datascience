# NBCAdids.py
#
# NBC is going to give us a list of ad ids
# from this we need to get
#
# * a showType retention score for each adid
# * a non-showType retentention score
# * an estimated retention score if the two above are not applicable
#   (i.e. first time airing)
#
# ** Returns **
# rank order, also put a standard deviation on this so we can guarantee
#  we have the best ads
# how many times this ad has aired before
#
# LKS, January 2017
#
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
from sqlalchemy import create_engine
import json
import seaborn as sns
#
#
#
# list of adids
testData=pd.read_csv('ChicagoMed_20170302.csv', sep=';', header=0)
adids=testData.adid.unique()
title='Chicago Med'

showGoal='Drama & Action'
podletterGoal='None'
day_part=6
dirName='Test3'
datebegin='20170215'
dateend='20170301'


uniShows=['15 Unforgettable Hollywood Tragedies', '1st Look',
       '20 Most Shocking Unsolved Crimes',
       '2017 Golden Globe Arrivals Special', '2017 NHL All-Star Game',
       'Air Racing', 'Amazing Facts Presents',
       'Amazing Facts Presents with Doug Batchelor',
       'American Ninja Warrior', 'Bad Girls Club',
       'Bad Girls Club: Casting Special',
       'Before They Were Housewives: Luann', 'Best Pan Ever!', 'Blindspot',
       'Botched', 'Bridal to Homicidal', 'Burn Notice',
       'CIZE Dance Workout!', 'CSI: Crime Scene Investigation',
       'Chicago Fire', 'Chicago Med', 'Chicago P.D.',
       'Chrisley Knows Best', 'Colony',
       'Countdown to the Red Carpet: The 2017 Academy Awards',
       'Dateline NBC', 'David Jeremiah', 'Days of our Lives',
       "Don't Be Tardy ...", "Don't Be Tardy for the Wedding",
       'E! After Party', 'E! Countdown to the Red Carpet',
       'E! Investigates', 'E! Live From the Red Carpet', 'E! News',
       'E! News Weekend', 'Early Today', 'Emerald City',
       'English Premier League Soccer', 'FIS Alpine Skiing', 'Face Off',
       'Fashion Police', 'Figure Skating', 'First Family of Hip Hop',
       'Flat and Sexy Abs!', 'Football Night in America',
       'Friday Night Tykes', "Girlfriends' Guide to Divorce", 'Grimm',
       'High School Football', 'Hockey Day in America Pregame',
       'Horse Racing', 'House', 'I Survived a Serial Killer', 'Imposters',
       'In Depth With Graham Bensinger',
       'In Touch With Dr. Charles Stanley', 'Incorporated',
       'Incredible Dog Challenge National Finals',
       'Inside the Actors Studio', 'International Auto Show',
       'It Takes a Killer', 'Joel Osteen',
       'Keeping Up With the Kardashians', 'Killer Couples', 'Killerpost',
       'Killision Course', 'Kourtney & Khlo\xc3\xa9 Take Miami',
       'Ladies of London', 'Last Call With Carson Daly',
       'Last Squad Standing', 'Late Night With Seth Meyers',
       'Law & Order: Criminal Intent', 'Law & Order: Special Victims Unit',
       "Mariah's World", 'Married to Medicine', 'Mecum Auto Auctions',
       'Meet the Press', 'Modern Family',
       'Modern Living with kathy ireland', 'Mom is 57, Looks 27!',
       'NBC Nightly News With Lester Holt', 'NCIS', 'NCIS: Los Angeles',
       'NHL Pregame Show', 'Open House NYC', 'PGA Tour Golf',
       'Perricone MD Cold Plasma Sub-D', 'Peter Popoff Ministries',
       'Policewomen Files', 'Powerless', 'Premier League Goal Zone',
       'Presidential Address', 'Presidential Inauguration 2017',
       'Red Carpet Rundown', 'Revenge Body With Khlo\xc3\xa9 Kardashian',
       'Rob & Chyna', 'Saturday Night Live', 'Sex and the City', 'Shooter',
       'Sisterhood of Hip Hop', 'Skiing', 'Snapped',
       'Snapped: Killer Couples', 'Snowboarding', 'So Cosmo',
       'Stevie Wonder, The Jackson 5, Marvin Gaye', 'Stop Smoking Now',
       'Strut', 'Suffering From Lower Back Pain?', 'Suits', 'Summer House',
       'Sunday Today With Willie Geist', 'Superstore', 'Taken',
       'Tattoos After Dark', 'The 128th Tournament of Roses Parade',
       'The 74th Annual Golden Globe Awards',
       'The Battle of the Ex-Besties', 'The Blacklist',
       'The Blacklist: Redemption', 'The Bravos',
       'The E! True Hollywood Story', 'The Ellen DeGeneres Show',
       'The Expanse', 'The Expanse: Expanded', 'The Good Place',
       'The Magicians', 'The New Celebrity Apprentice',
       "The Paley Center Salutes NBC's 90th Anniversary",
       'The Real Housewives Top 20 Reunion Moments',
       'The Real Housewives of Atlanta',
       'The Real Housewives of Beverly Hills', 'The Royals',
       'The Tonight Show Starring Jimmy Fallon', 'The Twilight Zone',
       'The Voice', 'The Voyager With Josh Garcia', 'The Wall',
       'This Is Us', 'Three Days to Live', 'Tia & Tamera',
       'Timber Creek Lodge', 'Timeless', 'Today', 'Today 2', 'Today 3',
       'Top Chef', 'Total Divas', 'Track and Field', 'Vanderpump Rules',
       'Volleyball', 'WWE Monday Night RAW', 'WWE SmackDown!',
       'Watch What Happens Live', 'Wilderness Vet']
Labels=[ 8,  1,  8,  1,  2,  7,  1,  5,  6,  8,  8, 11,  9,  6, 12,  8,  4,
        1,  4,  3,  3,  6, 12,  8,  4,  1,  5,  1,  8,  8,  1,  8,  8,  8,
       12, 12,  1,  3,  1,  1,  8,  8,  3,  4,  1,  2,  8,  4,  2,  3,  1,
        7, 12, 11,  4,  6,  5, 12,  2,  8,  2,  8,  9, 12,  8,  8,  8,  8,
       12,  1,  8,  1, 12,  4, 12,  4,  2,  6,  8,  1,  1,  1,  4,  8, 10,
        1,  1,  1, 11, 11,  8,  1,  5,  1,  5, 12,  8,  1, 12, 12,  8,  1,
        4,  8,  1,  8,  9,  1,  8,  5,  4, 12,  1,  3,  2,  8,  2,  6,  8,
        5,  1,  8,  8,  1,  8,  8,  6,  8,  1,  5, 11,  4,  4, 12,  1, 12,
        2,  5,  3,  8,  1,  8, 12,  0,  1,  1,  1,  4, 12,  0,  4,  8, 12,
        4,  4,  5]

Labels=np.array(Labels)
uniShows=np.array(uniShows)
goalLabel=Labels[uniShows==title]
#
#
# create the engines
def create_db_engine(db_type, host, port, database, user, password):
    return create_engine(db_type + '://' + user + ':' + password + '@' + host + ':' + port + '/' + database)


def create_db_engine_with_credentials(db_cred_file_name, conn_name):
    with open(db_cred_file_name) as json_file:

        db_credentials = json.load(json_file)
        conn = db_credentials[conn_name]
        # print type(conn['port'])
        return create_db_engine(conn['db_type'], conn['host'], conn['port'], conn['database'],
                                conn['user'], conn['password'])

redshift_engine = create_db_engine_with_credentials('/audience-core/credentials/db_credentials.json', 'ispot_redshift')
mysql_engine = create_db_engine_with_credentials('/audience-core/credentials/db_credentials.json', 'ispot_db_mysql')


def queryAd(adID):
    # input
    # adID = an iSpot adID
    #
    # returns
    # a pandas dataframe to use to calculate a tune out rate 
    #
    # give an ad id to calculate it's retention score
    query=\
    """ SELECT sc.name showType,
               series.title series_title,
               occ.Segment as PodNum,
               occ.SegmentOrder as Spot,
               
               avd.start_total_impressions, 	  
               avd.total_impressions,
               avd.start, 
               avd.first_qt, 
               avd.second_qt, 
               avd.third_qt, 
               avd.full,  
               ama.ad_title,            
               sched.program_airing_id,               
               occ.OccurenceID, 
               occ.pod_letter,
               mdsd.day_part_id day_part, 
               ama.duration
    
        FROM ispot_data.master_dsd mdsd
        JOIN ispot_data.ad_meta_national ama on mdsd.adid = ama.adid
        JOIN ispot_data.airings_detail ad on mdsd.dsd_id = ad.dsd_id
        JOIN ispot_data.audience_detail_occurrence ado on ad.occurrenceid = ado.occurrenceid
        JOIN ispot_data.audience_view_detail avd on avd.occurrenceid = ado.occurrenceid
        JOIN ispot_db.series series on mdsd.seriesid=series.seriesid
        JOIN ispot_db.seriescategory programs on mdsd.seriesid=programs.seriesid
        JOIN ispot_db.schedules sched on mdsd.scheduleid=sched.scheduleid
        JOIN ispot_db.showcategories sc on programs.showcategoryid = sc.showcategoryid
        RIGHT JOIN ispot_db.adoccurence occ on ado.occurrenceid = occ.OccurenceID
        WHERE  mdsd.date_est BETWEEN {0} AND {1}
        AND avd.start_total_impressions > 20
        AND ama.adid = {2}; """.format(datebegin, dateend, adID)
    Q1 = pd.read_sql_query(query, con=mysql_engine, coerce_float=False)
    return Q1


def get_tune_out(row):
  #
  # input
  # row = row of pandas dataframe
  #
  # output
  # tuneout rate row and other things
  #
  if row.start == 0:
    # redistribute the wealth
    tstart = (.7*row.first_qt)+(.2*row.second_qt)+(.01*row.third_qt)+\
             (.01*row.full)
    tfirst_qt=row.first_qt- .7*row.first_qt
    tsecond_qt=row.second_qt-.2*row.second_qt
    tthird_qt=row.third_qt- .01*row.third_qt
    tfull =row.full- .01*row.full
    
    # calculate tune out rate from this 
    if (tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (1-((tfull+tthird_qt)/row.start_total_impressions))
        method=5
    elif (tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (1-((tfull+tthird_qt+tsecond_qt)/row.start_total_impressions))
        method=6
    elif (tfirst_qt+tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (1-((tfull+tthird_qt+tsecond_qt+tfirst_qt)/row.start_total_impressions))
        method=7
    else:
        r = np.array([ tfirst_qt, tsecond_qt, tthird_qt, tfull])
        c=np.max((r/row.start_total_impressions))
        tune_out = 100*(c)                  
        method=8 # the worst method    
    
  else:       
    # the best data 
    if (row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (1-((row.full+row.third_qt)/row.start_total_impressions))
        method=1
    elif (row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (1-((row.full+row.third_qt+row.second_qt)/row.start_total_impressions))
        method=2
    elif (row.first_qt+row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (1-((row.full+row.third_qt+row.second_qt+row.first_qt)/row.start_total_impressions))
        method=3   
    else:    
        r = np.array([ row.first_qt, row.second_qt, row.third_qt, row.full])
        c=np.max((r/row.start_total_impressions))
        tune_out = 100*(c)                  
        method=4
  if np.isnan(tune_out) == True:
    tune_out=-1
  tune_out=100-int(100*tune_out)/100.
  try:
      return pd.Series({'method': method, 'tune_out': tune_out, 'podNum':row.PodNum, \
                    'spot':row.Spot, 'showType':row.showType, 'ad_title':row.ad_title, \
                    'series_title':row.series_title, 'pod_letter':row.pod_letter, 'day_part':row.day_part, \
                    'duration':row.duration})
  except:
      return pd.Series({'method': method, 'tune_out': tune_out, 'adid': row.adid})
#
#
def num2float(df):
    for col in df.columns:
        try:
            df[col] = df[col].astype(float)
        except:
            print(col+' column cannot be float-ized')
    return df


#
#
# START THE SCRIPT 
#
#
#
# make the directory to save everything
#
if not os.path.exists(dirName):
    os.makedirs(dirName)

#
#
# loop through ads
os.chdir(dirName)
file= open(title+'AdExamples.txt', 'wb')
file2=open(title+'tuneOuts.txt', 'wb')

testData=num2float(testData)
dfTest=testData.apply(get_tune_out, axis=1)

HitsSC=[]
HitsCluster=[]

OffSC=[]
OffCluster=[]


for iAd in range(len(adids)):
    print('Ad id is: '+ str(adids[iAd]))
    # first get the historical data for this 
    df= queryAd(adids[iAd])

    # then calculate tune out rates 
    df=num2float(df)
    
    dfproc=df.apply(get_tune_out, axis=1)
    testResult=dfTest[dfTest.adid==adids[iAd]]
    try:
        dfproc=dfproc[(dfproc.method !=4) & (dfproc.method!=8) & (dfproc.method!=3) & (dfproc.method!=7)]
    except:
        print('No ads')
    
    # cascade now
    #
    # first try by show type
    try:
     if len(dfproc[dfproc.showType==showGoal]) > 2:
        temp1=dfproc[dfproc.showType==showGoal]
        temp2=dfproc[dfproc.series_title.isin(uniShows[Labels==goalLabel])]
        
        showTypeCheck='yes'
        # check for pod letter 
        if len(temp1[temp1.day_part == day_part]) > 2:
            ndf=dfproc[(dfproc.showType==showGoal) & (dfproc.day_part == day_part)]
            ndf2=dfproc[(dfproc.series_title.isin(uniShows[Labels==goalLabel])) & (dfproc.day_part == day_part)]
            plcheck='yes'
        else:
            ndf=dfproc[(dfproc.showType==showGoal) ]
            ndf2=dfproc[dfproc.series_title.isin(uniShows[Labels==goalLabel])]
            plcheck='no'

     else:
        showTypeCheck='no'
        if len(dfproc[dfproc.day_part == day_part]) > 2:
            plcheck='yes'
            ndf=dfproc[dfproc.day_part == day_part]
            ndf2=dfproc[dfproc.day_part == day_part]

        else:
            plcheck='no'
            ndf=dfproc
            ndf2=dfproc
 
     print('Show Type Check: ' + str(showTypeCheck) + ',Day Part Check: '+ str(plcheck))
     if ndf['tune_out'].mean() != ndf2['tune_out'].mean():
           print('Viewability score is: '+ str(ndf['tune_out'].mean()))
           print('Standard deviation is: '+str(ndf['tune_out'].std()))
           print('Viewability score is: '+ str(ndf2['tune_out'].mean()))
           print('Standard deviation is: '+str(ndf2['tune_out'].std()))
           print('Actual Viewability is: '+ str(testResult.tune_out.mean()))
         
#     print('Show Type Check: ' + str(showTypeCheck) + ',Day Part Check: '+ str(plcheck))
#     print('Using Show Type, with ' + str(len(ndf)) + ' occurrences')
#     print('Viewability score is: '+ str(ndf['tune_out'].mean()))
#     print('Standard deviation is: '+str(ndf['tune_out'].std()))
#     #
#     print('And for using Show Clusters')
#     print('Viewability score is: '+ str(ndf2['tune_out'].mean()))
#     print('Standard deviation is: '+str(ndf2['tune_out'].std()))     

#     if plcheck == 'yes':
#        print('alternative outcome')
#        if showTypeCheck=='yes':
#            ndf=dfproc[dfproc.showType == showGoal]
#            
#        else:
#            ndf=dfproc
#        print('Viewability score is: '+ str(ndf['tune_out'].mean()))
#        print('Standard deviation is: '+str(ndf['tune_out'].std()))
    
    
    #
    # get the pod letter distribution
     my_dict = {i:list(ndf['pod_letter']).count(i) for i in np.array(ndf['pod_letter'])}
     print(my_dict)
     my_dict2 = {i:list(ndf['day_part']).count(i) for i in np.array(ndf['day_part'])}
     print('day part: '+ str(my_dict2))

     file.write(str(np.array(ndf.ad_title)[0]))
     file.write('\n')
     file.write(str(adids[iAd]))
     file.write('\n')
     file.write(str(ndf['tune_out'].mean()))
     file.write('\n')
     file.write(str(ndf['tune_out'].std()))
     file.write('\n')
     file.write(str(ndf2['tune_out'].mean()))
     file.write('\n')
     file.write(str(ndf2['tune_out'].std()))
     file.write('\n')
     file.write('Actual: '+str(testResult.tune_out))
     file.write('\n')                                           
     
     #file.write(str(my_dict))
     #file.write('\n')
     file.write('count: '+str(len(ndf['tune_out'])))
     file.write('\n')
     file.write('duration: '+str(np.array(ndf.duration)[0]))
     file.write('\n')
     file.write('\n')


     
     file2.write(str(ndf['tune_out'].mean()))
     file2.write('\n')

     TRT=testResult.tune_out.mean()
     STD=ndf.tune_out.std()
     STD2=ndf2.tune_out.std()
     if ((ndf.tune_out.mean() + STD) >= TRT) & ( (ndf.tune_out.mean() - STD ) <= TRT):
         HitsSC+=[1]
     else:
         HitsSC+=[0]
         
     if ((ndf2.tune_out.mean() + STD2) >= TRT) & ( (ndf2.tune_out.mean() - STD2 ) <= TRT):
         HitsCluster+=[1]
     else:
         HitsCluster+=[0]        
  

     OffSC+= [ndf.tune_out.mean() - TRT]
     OffCluster+= [ndf2.tune_out.mean() - TRT ]
     HitsCluster=np.array(HitsCluster)
     HitsSC=np.array(HitsSC)
     len(HitsSC[HitsSC==1])
     len(HitsCluster[HitsCluster==1])


     
     # tomorrow test to see if cascade with pod letter or day part is worth it 
     #podLetter_tuneOuts=ndf.groupby('pod_letter')['tune_out']
     #mPL=podLetter_tuneOuts.mean()
     #stdPL=podLetter_tuneOuts.std()
     # pod letter definitely worth it
     
     #os.chdir(dirName)    
     sns.set_style("whitegrid")   
     ax = sns.barplot(x="day_part", y="tune_out", data=ndf)
     ax.set(ylim=(60,100))
     plt.savefig('adid='+str(adids[iAd])+'_viewabilityScore.png')
     plt.close()
     #os.chdir('..')
    except:
       file2.write('np.nan')
       file2.write('\n') 
       None
file.close()
file2.close()
os.chdir('..')
