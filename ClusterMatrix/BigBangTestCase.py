# BigBangTestCase.py
#
# try to get the big bang theory similar shows to beat the show type
#
# LKS April 2017
#
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import glob
#
#

# first load in the csv
os.chdir('BBT')
#allFiles = glob.glob("BBT.csv")
frame = pd.DataFrame()
list_ = []
data=pd.read_csv('BBT3.csv', error_bad_lines=False, header=0, delimiter=';')
os.chdir('..')

#data=pd.read_csv('broadcastChannels.csv', delimiter=';', header=0)
data=data.dropna(axis=0)

# drop shows where the title contains 'news' 
data=data[data['previous_content_title'].str.contains("news") == False]
data=data[data['previous_content_title'].str.contains("News") == False]
data=data[data['previous_content_title'].str.contains("NBC") == False]
data=data[data['previous_content_title'].str.contains("CBS") == False]
data=data[data['previous_content_title'].str.contains("FOX") == False]

data=data[(data['count'] >= 10)]
#
uniShow0=data['previous_content_title'].unique()
SimilarityMatrix=np.ones(len(uniShow0))
#
# now compare with Big Bang
for iShow in range(len(uniShow0)):
        matchAds=pd.merge(data[data['previous_content_title'] == uniShow0[iShow]],
                          data[data['previous_content_title'] == 'The Big Bang Theory'],
                          how='inner', on=['ad_id'])
        x1=np.array(matchAds['avg_x']); x2=np.array(matchAds['avg_y'])
        diff=[ 100.*np.abs(x1[i]-x2[i])/((x1[i]+x2[i])/2.) for i in range(len(x1))]
        if len(diff) > 10:
            SimilarityMatrix[iShow]=np.nanmedian(diff)
            
            
        else:
            SimilarityMatrix[iShow]=np.nan
            
       
        #SimilarityMatrix[iShow][iShow2]=cosine_similarity(matchAds.avg_x, matchAds.avg_y)[0][0]
        print('show is: '+str(uniShow0[iShow]))
        print(str(SimilarityMatrix[iShow]) + ' length is ' + str(len(diff))  )

ShowList=[]
ShowP=[]

pp=np.nanpercentile(SimilarityMatrix, 10)
shows=uniShow0[SimilarityMatrix<=pp]
per=SimilarityMatrix[SimilarityMatrix<=pp]

