# audienceCluster.py
#
# try going from device level to profile similar shows by ad viewing behavior
#
# LKS March 2017
#
#
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
#
# first load in the csv
data=pd.read_csv('broadcastChannels.csv', delimiter=';', header=0)
data=data.dropna(axis=0)

# drop shows where the title contains 'news' 
data=data[data.title.str.contains("news") == False]
data=data[data.title.str.contains("News") == False]
data=data[data.title.str.contains("NBC") == False]
data=data[data.title.str.contains("CBS") == False]
data=data[data.title.str.contains("FOX") == False]
#
# get the unique shows, first round
#adIds=data['adid'].unique()
uniShow0=data['title'].unique()
for iUni in uniShow0:
    if len(data[data.title == iUni]) < 200:
        data=data[data.title != iUni]

# second round after the adjustment 
uniShow=data['title'].unique()
SimilarityMatrix=np.ones((len(uniShow), len(uniShow)))*np.nan

for iShow in range(len(uniShow)):
    for iShow2 in range(iShow+1, len(uniShow)):
        # find where the shows overlap
        matchAds=pd.merge(data[data.title == uniShow[iShow]],
                          data[data.title == uniShow[iShow2]],
                          how='inner', on=['adid'])
        x1=np.array(matchAds.avg_x); x2=np.array(matchAds.avg_y)
        diff=[ 100.*np.abs(x1[i]-x2[i])/((x1[i]+x2[i])/2.) for i in range(len(x1))]
        if len(diff) > 30:
            SimilarityMatrix[iShow][iShow2]=np.nanmedian(diff)
            SimilarityMatrix[iShow2][iShow]=np.nanmedian(diff)
            
        else:
            SimilarityMatrix[iShow][iShow2]=np.nan
            SimilarityMatrix[iShow2][iShow]=np.nan
       
        #SimilarityMatrix[iShow][iShow2]=cosine_similarity(matchAds.avg_x, matchAds.avg_y)[0][0]
        print(str(SimilarityMatrix[iShow][iShow2]) + ' length is ' + str(len(diff))  )

#
# get the similar shows
import pickle
textFile=open('similarShows.p', 'wb')

for checker in range(len(uniShow)):
    print('Show is: '+ str(uniShow[checker]))
    pp=np.nanpercentile(SimilarityMatrix[checker], 10)
    #cc=np.argsort(SimilarityMatrix[checker])
    cc=SimilarityMatrix[checker]
    shows=uniShow[cc<=pp]
    ShowList.append([uniShow[checker]]+list(shows))

pickle.dump(ShowList, textFile)
    
   


