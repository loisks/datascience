# ClusterMatrix.py
#
# the outcome of our this is us test!
#
# LKS, February 2017
#
#
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
from scipy.stats import ttest_ind
from scipy.stats import pearsonr
import matplotlib.dates as mdates
from sklearn.metrics.pairwise import cosine_similarity
import pickle

def get_tune_out(row):
  #
  # input
  # row = row of pandas dataframe
  #
  # output
  # tuneout rate row and other things
  #
  if row.start == 0:
    # redistribute the wealth
    tstart = (.7*row.first_qt)+(.2*row.second_qt)+(.01*row.third_qt)+\
             (.01*row.full)
    tfirst_qt=row.first_qt- .7*row.first_qt
    tsecond_qt=row.second_qt-.2*row.second_qt
    tthird_qt=row.third_qt- .01*row.third_qt
    tfull =row.full- .01*row.full
    
    # calculate tune out rate from this 
    if (tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt)/(1.0*row.start_total_impressions)))
        method=5
    elif (tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt+tsecond_qt)/(1.0*row.start_total_impressions)))
        method=6
    elif (tfirst_qt+tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt+tsecond_qt+tfirst_qt)/(1.0*row.start_total_impressions)))
        method=7
    else:
        r = np.array([ tfirst_qt, tsecond_qt, tthird_qt, tfull])
        c=1.0-np.max((r/row.start_total_impressions))
        tune_out = 100.0*(c)                  
        method=8 # the worst method    
    
  else:       
    # the best data 
    if (row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt)/(1.0*row.start_total_impressions)))
        method=1
    elif (row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt+row.second_qt)/(1.0*row.start_total_impressions)))
        method=2
    elif (row.first_qt+row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt+row.second_qt+row.first_qt)/(1.0*row.start_total_impressions)))
        method=3   
    else:    
        r = np.array([ row.first_qt, row.second_qt, row.third_qt, row.full])
        c=1.0-np.max((r/(1.0*row.start_total_impressions)))
        tune_out = 100.0*(c)                  
        method=4
  if np.isnan(tune_out) == True:
    tune_out=-1

  tune_out=int(100*tune_out)/100.
  return pd.Series({'method': method, 'tune_out': tune_out, 'ad_title':row.title, \
                    'series_title':row.series_title,   'start_total_impressions':row.start_total_impressions, 'total_impressions':row.total_impressions })


## initial process ##
## takes a very long time ## 
date1='20170101'
date2='20170301'


data=pd.read_csv(date1+'_'+date2+'_matrix.csv', sep=';', header=0)
#
df=data.apply(get_tune_out, axis=1)
df=df[(df.method != 4) & (df.method != 8)]
df=df[(df.method != 3) & (df.method != 7)]
df=df[(df.tune_out != 99.0)]
#
df2=df[['ad_title', 'series_title', 'tune_out']]
df2=df2.sort_index(by='series_title')
uniAds=df2.ad_title.unique()
uniShows=np.sort(df2.series_title.unique())

Differences= [ [ [] for ijk in range(len(uniShows))] for jki in range(len(uniShows))]

for iAd in range(len(uniAds)):
   print(100*iAd/(1.0*len(uniAds)))
   dftemp=df2[(df2.ad_title == uniAds[iAd])]
   showMatches1=dftemp.series_title.unique()   
   matchedIndicies1=[i for i, item in enumerate(uniShows) if item in set(showMatches1)]
   # iterate over matches   
   for iShow in matchedIndicies1:
     for iShow2 in matchedIndicies1:
        temp1=dftemp[dftemp.series_title == uniShows[iShow]]
        temp2=dftemp[dftemp.series_title == uniShows[iShow2]]    
        Differences[iShow][iShow2]+=[np.abs(temp1.tune_out.mean() - temp2.tune_out.mean())]
   

pickle.dump(Differences, open(date1+'_'+date2+'_DifferencesTuneOuts.p', 'wb'))

#### STOPPING POINT ######

Differences=pickle.load(open(date1+'_'+date2+'_DifferencesTuneOuts.p', 'rb'))

upDiff=[ [ 0 for ijk in range(len(uniShows))] for jki in range(len(uniShows))]
for iiShow in range(len(uniShows)):
  for jjShow in range(len(uniShows)):
    upDiff[iiShow][jjShow]=np.nanmean(Differences[iiShow][jjShow])

#agg=[]
#for item in range(len(upDiff)):
#  agg+=[len(np.where(np.isnan(np.array(upDiff)[item])==True)[0])]
#
#agg=np.array(agg)
#rShows=np.array(uniShows)[np.where(agg<250)[0]]
#rupDiff=np.array(upDiff)[np.where(agg<250)[0]]
#rupDiff=np.swapaxes(rupDiff, 1,0)[np.where(agg<250)[0]]
rShows=uniShows
rupDiff=upDiff

# plot?
import matplotlib.pyplot as plt
from sklearn.cluster import AgglomerativeClustering
from scipy.cluster.hierarchy import linkage
import scipy

replaceNans=np.where(np.isnan(rupDiff)==True)
upDiff2= np.array(rupDiff)
upDiff2[replaceNans]=np.nanmedian(rupDiff)+1

Z=linkage(upDiff2, method='ward')
scipy.cluster.hierarchy.fcluster(Z, .1, criterion='inconsistent', depth=2, R=None, monocrit=None)

bins=13
ward = AgglomerativeClustering(n_clusters=bins, linkage='ward').fit(upDiff2)
label = ward.labels_

fig=plt.figure()
plt.hist(label, bins=range(1,bins+1))
plt.title('bins = '+str(bins))
plt.show()
  


#with file('results_'+str(date1)+'_'+str(date2)+'.txt', 'w') as outfile:
#for ibin in range(bins):
  
