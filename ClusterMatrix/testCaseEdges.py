# testCaseScorpion.py
#
# test out if profiling the shows by ad viewability works or not
#
# LKS, March 2017
#
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
import pickle
import json
import string
from sqlalchemy import create_engine
from ast import literal_eval

#LOIS
# INCORPORATE NEW METHOD OTMORROW! 

# KEEP ON IMPROVING FOR COLONY !
def is_ascii(s):
    try:
        s.decode('ascii')
        return True
    except UnicodeDecodeError:
        return False


#showName='The Simpsons'
names=['Blindspot',
       'Chicago Fire',
       'Chicago Justice',
       'Chicago Med',
       'Chicago P.D.',
       'Dateline NBC',
       'Emerald City',
       'Grimm',
       'Law & Order: Special Victims Unit',
       'Little Big Shots',
       'Neighbors',
       'Powerless',
       'Ride Along',
       'Saturday Night Live',
       'Shades of Blue',
       'Superstore',
       'Taken',
       'Tapping IN: The Happiest People and Places',
       'The Blacklist',
       'The Blacklist: Redemption',
       'The Good Place',
       'The New Celebrity Apprentice',
       'The Voice',
       'The Wall',
       'This Is Us',
       'Timeless']
# now enter appropriate query for getting the modified show type data
date1='2017-02-01'
date2='2017-03-25'

dateTest1='2017-03-25'
dateTest2='2017-04-26'

myfile = open('TestResults_NBCPrimeTime.txt', 'wb')
for showName in names:
            
     def create_db_engine(db_type, host, port, database, user, password):
         return create_engine(db_type + '://' + user + ':' + password + '@' + host + ':' + port + '/' + database)
     
     
     def create_db_engine_with_credentials(db_cred_file_name, conn_name):
         with open(db_cred_file_name) as json_file:
             db_credentials = json.load(json_file)
             conn = db_credentials[conn_name]
             # print type(conn['port'])
             return create_db_engine(conn['db_type'], conn['host'], conn['port'], conn['database'],
                                     conn['user'], conn['password'])
     redshift_engine = create_db_engine_with_credentials('/audience-core/credentials/db_credentials2.json', 'ispot_redshift')
     mysql_engine = create_db_engine_with_credentials('/audience-core/credentials/db_credentials2.json', 'ispot_db_mysql')
     
     def apostropheFix(arrayOfNames):
            arrayOfNames=arrayOfNames.replace("'s" ,"''s")
            arrayOfNames=arrayOfNames.replace("'70" ,"''70") 
            arrayOfNames=arrayOfNames.replace("O'N", "O''N")
            arrayOfNames=arrayOfNames.replace("'re", "''re")
            arrayOfNames=arrayOfNames.replace("n't", "n''t")
            arrayOfNames=arrayOfNames.replace("s' ", "s'' ")
            arrayOfNames=arrayOfNames.replace(" N' ", " N'' ")
           

            #arrayOfNames=arrayOfNames.replace('\xf3', 'o')
            #arrayOfNames=arrayOfNames.replace('\xc3\xa9', 'oe')
            #arrayOfNames=arrayOfNames.replace('\xf1os', 'os')
            
            #arrayOfNames=arrayOfNames.replace("'M", "''M")
            #arrayOfNames=arrayOfNames.replace("'The Incredible Dr. Pol", "''The Incredible Dr. Pol")
            return(arrayOfNames)
     
     def dataFetch(date1, date2, tt):
         query=\
                """
                SELECT adid, 
	        AVG(CASE WHEN (datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0) <= duration_sec 
                THEN 100.0*(datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0)/(1.0*duration_sec)
	        WHEN (datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0) > duration_sec AND
		(datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0) <= 1.10*duration_sec THEN
		100.0*(datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0)/
			CEIL((datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0))
		WHEN duration_sec < 16 and (datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0) <= 18 
                AND (datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0) > duration_sec
                THEN 100.0*(datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0)/
		    CEIL((datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0))
		 END) new_pv,
	         addu.duration_sec, count(distinct occurrence_id)

                FROM ap_results_production_output_inferencing_60_day_view_lois inf
                JOIN ispot_db.adoccurence ado on occurrence_id = ado.occurenceid
                JOIN ispot_db.schedules sched on ado.scheduleid = sched.scheduleid
                JOIN ispot_db.ads_subset addu on ado.adid = addu.ad_id
                JOIN ispot_db.programs series on sched.program = series.tmsid
                WHERE series.title in ('{2}')
                AND est_occurencedatetime BETWEEN '{0} 00:00:00.000000' AND '{1} 00:00:00.000000'
                AND (datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0)  < 1.10*duration_sec
                AND percent_viewed IS NOT NULL
                AND start_media_time < 4000
                GROUP BY adid, duration_sec
                """.format(date1, date2, tt[1:-1])              
         return( pd.read_sql_query(query, con=redshift_engine, coerce_float=False)  )  
     
     
     # to automate you need to have it automatically do the queries and pull the data
     # first one that needs to happen is one to get the show type
     
     # first get the list of shows
     #SimilarShows=pickle.load(open('SimilarShows.p', 'rb'))
     # the first column is the index
     #iterable=len(SimilarShows)
     #names=[]
     #SimilarShowData=[]
     
     
     
     
     querySimilarShows= \
     """
                        SELECT ad_id, previous_content_title,
	AVG(CASE WHEN (datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0) <= inf.duration THEN
	100.0*(datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0)/(1.0*inf.duration)
	WHEN (datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0) > inf.duration AND
		(datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0) <= 1.10*inf.duration THEN
		100.0*(datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0)/
			CEIL((datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0))
		WHEN inf.duration < 16 and (datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0) <= 18 AND
				 (datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0) > inf.duration THEN
			100.0*(datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0)/
			CEIL((datediff(seconds,start_detection_timestamp,end_timestamp)+start_media_time/1000.0))

			END) , count(distinct occurrence_id)
     FROM (
        SELECT distinct ad_id
        FROM ap_results_production_output_inferencing_60_day_view_lois inf 
        JOIN ispot_db.adoccurence ado on occurrence_id = ado.occurenceid
        JOIN ispot_db.schedules sched on ado.scheduleid = sched.scheduleid
        JOIN ispot_db.programs series on sched.program = series.tmsid
        WHERE previous_content_title = '{0}'
        ---AND next_content_title = '{0}'
        AND percent_viewed IS NOT NULL
        AND sched.show_reach  ='N'  
        AND inf.duration > 25   
        AND series.series NOT LIKE 'IMV%%'
     ) distinct_bbt_adids
     JOIN ap_results_production_output_inferencing_60_day_view_lois inf USING (ad_id)
     JOIN ispot_db.adoccurence ado on occurrence_id = ado.occurenceid
     JOIN ispot_db.schedules sched on ado.scheduleid = sched.scheduleid
     JOIN ispot_db.programs series on sched.program = series.tmsid
     WHERE series.series NOT LIKE 'IMV%%'
       
       AND percent_viewed IS NOT NULL
       AND start_media_time < 4000
       AND sched.show_reach  ='N'  
       ---AND previous_content_title = next_content_title
       AND inf.duration > 25 
     GROUP BY ad_id, previous_content_title
      HAVING count(percent_viewed) > 20
      AND AVG(start_media_time) < 4000
     ORDER BY ad_id;
     """.format(showName)
     
     # that gets the list 
     data = pd.read_sql_query(querySimilarShows, con=redshift_engine, coerce_float=False)
     data=data.dropna(axis=0)
     
     # drop shows where the title contains 'news' 
     data=data[data['previous_content_title'].str.contains("news") == False]
     data=data[data['previous_content_title'].str.contains("News") == False]
     data=data[data['previous_content_title'].str.contains("NBC") == False]
     data=data[data['previous_content_title'].str.contains("CBS") == False]
     data=data[data['previous_content_title'].str.contains("FOX") == False]
     #data=data[(data['count'] >= 10)]
     #
     uniShow0=data['previous_content_title'].unique()
     SimilarityMatrix=np.ones(len(uniShow0))
     
     # now compare with Big Bang
     for iShow in range(len(uniShow0)):
         if len(data[data['previous_content_title'] == uniShow0[iShow]]) > 10:
             matchAds=pd.merge(data[data['previous_content_title'] == uniShow0[iShow]],
                               data[data['previous_content_title'] == showName ],
                               how='inner', on=['ad_id'])
             x1=np.array(matchAds['avg_x']); x2=np.array(matchAds['avg_y'])
             diff=[ 100.*float(np.abs(x1[i]-x2[i]))/(float(x1[i]+x2[i])/2.) for i in range(len(x1))]
             if len(diff) > 5:
                 SimilarityMatrix[iShow]=np.nanmedian(diff)                 
             else:
                 SimilarityMatrix[iShow]=np.nan
         else:
             SimilarityMatrix[iShow]=np.nan
                 
            
             #SimilarityMatrix[iShow][iShow2]=cosine_similarity(matchAds.avg_x, matchAds.avg_y)[0][0]
            
     print('Sorted Through Similarity Matrix')
     ShowList=[]
     ShowP=[]
     
     pp=np.nanpercentile(SimilarityMatrix, 8)
     SimilarShows=uniShow0[SimilarityMatrix<=pp]
     per=SimilarityMatrix[SimilarityMatrix<=pp]

     
     
     # SimilarShows give the top 10% of most similar shows 
     
     #myfile = open('TestResults2.txt', 'wb')
     #for item in range(iterable):
     #    names.append(SimilarShows[item][0])
     #    SimilarShowData.append(SimilarShows[item][1:])
     # ok now do quick queries to get the showType
     showTypes=[]
     #for item in range(iterable):
     #    showName=names[item]
     query = \
                 """
              select sc2.name
                 FROM ispot_db.seriescategory sc 
                 join ispot_db.series s on sc.seriesid = s.seriesid
                 JOIN ispot_db.showcategories sc2 on sc2.showcategoryid = sc.showcategoryid
                 WHERE s.title = "{0}"
                 LIMIT 1
                 """.format(showName)
     showType = pd.read_sql_query(query, con=mysql_engine, coerce_float=False)
     try:
             showTypes.append(showType.values[0][0])
     except:
             showTypes.append([])
     
     #
     #
     
     SimilarShows=[i.decode() for i in SimilarShows]
     SS=[s for s in SimilarShows if is_ascii(s)]
     tt=','.join(map("'{0}'".format, list(SS)))
     tt=apostropheFix(tt)
     
     # now we have the similar show data
     # get the show Type data now
     # first have to get a query with the show type data 
     #
     try:
        result=dataFetch(date1, date2, tt)
        queryShows=\
                 """
                 SELECT s.title series_title
              
                 FROM ispot_data.master_dsd mdsd 
                 JOIN ispot_db.schedules sch on sch.scheduleid=mdsd.scheduleid
                 JOIN ispot_db.programs p on sch.program=p.id
                 JOIN ispot_db.series s on s.seriesid=p.series
                 JOIN ispot_db.seriescategory sc1 on s.seriesid = sc1.seriesid
                 JOIN ispot_db.showcategories sc on sc1.showcategoryid = sc.showcategoryid
                 WHERE sch.show_reach = 'N'
                 AND s.title NOT LIKE '%%[Movie]%%'
                
                 AND sc.name = '{2}'
                 AND mdsd.date_est BETWEEN '{0}' AND '{1}'
                 """.format(date1, date2, showTypes[0])
        temp = pd.read_sql_query(queryShows, con=mysql_engine, coerce_float=False)
        seriesList=temp.series_title.unique()
       
        #
        # now we got the similar shows, quantify how many match between show type and similar shows
        matches=set(SimilarShows) & set(seriesList)
        percentMatch=200.0*len(matches)/(len(tt)+len(seriesList))
        print('Percent Match for '+ showName + ' is: '+str(int(100*percentMatch)/100.))

        SL= [s for s in seriesList if is_ascii(s)]
        SLmod=[]
        for item in SL:
             try:
                  item=item.replace("'s", "''s")
                  qS=\
                              """
                              SELECT title 
                              FROM ispot_db.series 
                              WHERE title = '{0}'
                              """.format(item)
                  r = pd.read_sql_query(qS, con=mysql_engine, coerce_float=False)
                  SLmod.append(item)
             except:
                  None
                  
        
        dd=','.join(map("'{0}'".format, list(SLmod)))
        #dd=apostropheFix(dd)
        resultShowTypes=dataFetch(date1, date2, dd)
        tname=showName
        tname=apostropheFix(tname)      
        #resultShow=dataFetch(date1, date2,"'{0}'".format(tname))
        resultTestShow=dataFetch(dateTest1, dateTest2, "'{0}'".format(tname))
     
        # think it would be good to clean up some of the fishy results first
        resultTestShow=resultTestShow[resultTestShow.stddev != 0]
        resultTestShow=resultTestShow.dropna(axis=0)
        resultShowTypes=resultShowTypes[resultShowTypes.stddev != 0]
        resultShowTypes=resultShowTypes.dropna(axis=0)
        result=result[result.stddev != 0]
        result=result.dropna(axis=0)
     
        # great, now run the analysis part
        dfs=[resultTestShow, result , resultShowTypes]
        df_final = reduce(lambda left,right: pd.merge(left,right,on='adid'), dfs)
     
        diff=np.abs(np.array(df_final['avg_x'])-np.array(df_final['avg_y']))# select
        diff2=np.abs(np.array(df_final['avg_x'])-np.array(df_final['avg'])) # show Type
     
        diff=diff[diff<8]
        diff2=diff2[diff2<8]
     
        print('Show is: ' + showName +'\n')
        print('Similar show diff is: '+ str(int(100*float(np.nanmean(diff)))/100.) +'\n')
        print('similar show std is: '+str(int(100*float(np.std(diff)))/100.))
        print('Show Type diff is: '+ str(int(100*float(np.nanmean(diff2)))/100.) +'\n')
        print('Show Type std is: '+str(int(100*float(np.std(diff2)))/100.))

        # write to a file
        myfile.write('Show is: ' + str(showName) +'\n')
        myfile.write('Similar show diff is: '+ str(int(100*float(np.nanmean(diff)))/100.) +'\n')
        myfile.write('Similar show diff std is: '+ str(int(100*float(np.std(diff)))/100.) +'\n')
        myfile.write('Show Type diff is: '+ str(int(100*float(np.nanmean(diff2)))/100.) +'\n')
        myfile.write('Show Type diff std is: '+ str(int(100*float(np.std(diff2)))/100.) +'\n')
        myfile.write('Percentage Match is: '+str(int(100*percentMatch)/100.) +'\n')
     
     except(None):
        print('error: '+showName)

    
myfile.close()


    
    

# first load in the csv
#dataScorp=pd.read_csv('ChicagoMedResults.csv', delimiter=';', header=0)
#dataShowType=pd.read_csv('ChicagoMedShowTypes.csv', delimiter=';', header=0)
#dataSelect=pd.read_csv('ChicagoMedSimilarShows.csv', delimiter=';', header=0)
#
#
#CorrectAds=dataScorp.adid.unique()
#
#
## merge
##df=pd.concat([dataScorp, dataShowType, dataSelect], axis=1,keys=['adid'], join='inner', on)
#dfs=[dataScorp, dataSelect, dataShowType]
##df=dataScorp.merge(dataShowType,on='adid').merge(dataSelect,on='adid')
#df_final = reduce(lambda left,right: pd.merge(left,right,on='adid'), dfs)
#cc=pd.merge(dataScorp, dataSelect, on='adid', how='inner')
#dd=pd.merge(cc, dataShowType, on='adid', how='inner')
#
#diff=np.array(dd['avg_x'])-np.array(dd['avg_y'])# select
#diff2=np.array(dd['avg_x'])-np.array(dd['avg']) # show Type
