# timeStamps.py
#
# time to detection histogram
#
# LKS, June 2017
#
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
import matplotlib
#
#
file=pd.read_csv('timeStampsByInfo.csv', delimiter=';')
dataDown=file[file.detection <= 4000]
dataUp=file[file.detection > 4000]
#data=np.array([float(i) for i in np.array(file['detection'])])
data=file['detection']
data=data[data>0]

check=dataUp.groupby('segment')
means=check.detection.mean()[0:10]
errors=check.detection.std()[0:10]

means.plot.bar(yerr=errors)
plt.savefig('detection_time_by_segment.pdf')

fig=plt.figure()
check=dataUp.groupby('segmentorder')
means=check.detection.mean()[0:10]
errors=check.detection.std()[0:10]

means.plot.bar(yerr=errors)
plt.savefig('detection_time_by_segmentorder.pdf')


# channel

fig=plt.figure()
check=dataUp.groupby('channel')
means=check.detection.mean()#[0:10]
errors=check.detection.std()#[0:10]
errors=errors[means.index.isin( [105,107, 104, 113]) ]
means=means[means.index.isin( [105,107, 104, 113]) ]
#errors=errors[means> 10000]
#means=means[means > 10000]
means.plot.bar(yerr=errors)
plt.savefig('detection_time_by_channel.pdf')



fig=plt.figure()
ax=fig.add_subplot(111)
ax.hist(data, bins=np.linspace(0, 20000, 401))
ax.set_title('Time to Detection')
ax.set_xlabel('Detection Time [ms]')
ax.set_ylabel('Number of Occurrences')
ax.axvline(x=4000, c='green')
ax.axvline(x=8000, c='red')
plt.savefig('DetectionTime.png')



data2=np.array([float(i) for i in np.array(file['time'][(file.duration>11) & ( file.duration < 19)][1:])])
data2=data2[data2>0]
# 15 seconds
fig=plt.figure()
ax=fig.add_subplot(111)
ax.hist(data2, bins=np.linspace(0, 20000, 401))
ax.set_title('Time to Detection')
ax.set_xlabel('Detection Time [ms]')
ax.set_ylabel('Number of Occurrences')
ax.axvline(x=4000, c='green')
ax.axvline(x=8000, c='red')
plt.savefig('DetectionTime_15s.png')


data3=np.array([float(i) for i in np.array(file['time'][(file.duration>20) & ( file.duration < 40)][1:])])
data3=data3[data3>0]
# 15 seconds
fig=plt.figure()
ax=fig.add_subplot(111)
ax.hist(data3, bins=np.linspace(0, 20000, 401))
ax.set_title('Time to Detection')
ax.set_xlabel('Detection Time [ms]')
ax.set_ylabel('Number of Occurrences')
ax.axvline(x=4000, c='green')
ax.axvline(x=8000, c='red')
plt.savefig('DetectionTime_30s.png')

data4=np.array([float(i) for i in np.array(file['time'][(file.duration>40) & ( file.duration < 120)][1:])])
data4=data4[data4>0]
# 15 seconds
fig=plt.figure()
ax=fig.add_subplot(111)
ax.hist(data4, bins=np.linspace(0, 20000, 401))
ax.set_title('Time to Detection')
ax.set_xlabel('Detection Time [ms]')
ax.set_ylabel('Number of Occurrences')
ax.axvline(x=4000, c='green')
ax.axvline(x=8000, c='red')
plt.savefig('DetectionTime_60s.png')
