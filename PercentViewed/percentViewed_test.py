# percentViewed_test.py
#
# test out the percent viewed distributions
# start flagging for not good ads
#
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import matplotlib.dates as mdates
from textwrap import wrap
from scipy.stats import kendalltau
import seaborn as sns

#
#
percentileStrings=['95', '92', '90', '88', '85']
#percentileStrings=['88']
for ip in percentileStrings:
   percentile95=pd.read_csv(ip+'_cutoff_test.csv', sep=';', header=0).groupby('adid')['new_pv'].agg(['mean', 'count'])
   pp=percentile95[percentile95['count']>2]
   pp['mean'].hist(bins=np.arange(50,101,1))
   plt.savefig(ip+'_percentile_distribution_by_adid.png')
   plt.clf()
   print(pp['mean'].mean())
   print(pp[pp['mean']==100])

   # distributions
   minD=[10, 25, 50]
   maxD=[20, 35, 65]
   for ii in range(len(minD)):
       percentile95=pd.read_csv(ip+'_cutoff_test.csv', sep=';', header=0)
       p2=percentile95[(percentile95['duration_sec']>minD[ii]) & (percentile95['duration_sec']<maxD[ii])]
       p2=p2.groupby(['adid'])['new_pv'].agg(['mean', 'count'])
       pp=p2[p2['count']>2]
       pp['mean'].hist(bins=np.arange(50,101,1))
       plt.savefig(ip+'_percentile_distribution_by_duration='+str(np.mean([minD[ii], maxD[ii]]))+'_adid.png')
       plt.clf()
       print(pp['mean'].mean())
       print(pp[pp['mean']==100])


  # percentile95['new_pv'].hist(bins=np.arange(80,101,1))
  # plt.savefig(ip+'_high_percentile_distribution.png')
  # plt.clf()
  
