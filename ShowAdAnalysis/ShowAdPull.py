# ShowAdPull.py
#
# Python Script to make plots for overindexing by advertiser, brand, and ad title 
#
# LKS, February 2017
# updated April 2017
#
#
# list of imports used in script  
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import matplotlib.dates as mdates
from textwrap import wrap

#
# strings to fill in for analysis - these will need to be updated
# each time. This should be the only part that needs to be changed
#
title='Lifetime Financial'   # primary show name 
season=''               # if there is a particular season
daypart=''    # of if like Daypart 3, you'd put daypart = 'Day part = 3' here. 
channelName='Female Financial Competitive Set'   # the competition. This could be ABC Modern Family if more
                        # specific than others
specIndustries=[ 'Banks & Credit Cards',  'Credit Services', 'Debt Assistance Programs', 'Investment Services',  'Tax Filing Services', 'Loans']


# load in the csv files
# make sure the csv are semi-colon separated!!
#
dataAds=pd.read_csv('Lifetime_Financial.csv', sep=';', header=0)
dataShow=pd.read_csv('Others_Financial.csv', sep=';', header=0)
#dataShow=dataShow[dataShow.series_title == title]
#dataAds=dataAds[dataAds.series_title != title]


#
# functions, should not need modification 
#
def autolabel(rects, diff):
    """
    Attach a text label above each bar displaying its height
    """
    for i in range(len(rects)):
        height = rects[i].get_height()
        ax.text(rects[i].get_x() + rects[i].get_width()/2., .995*height,
                str(int(10*diff[i])/10.0) + ('%'),
                ha='left', va='bottom')

def get_tune_out(row):
  #
  # input
  # row = row of pandas dataframe
  #
  # output
  # tuneout rate row and other things
  #
  if row.start == 0:
    # redistribute the wealth
    tstart = (.7*row.first_qt)+(.2*row.second_qt)+(.01*row.third_qt)+\
             (.01*row.full)
    tfirst_qt=row.first_qt- .7*row.first_qt
    tsecond_qt=row.second_qt-.2*row.second_qt
    tthird_qt=row.third_qt- .01*row.third_qt
    tfull =row.full- .01*row.full
    
    # calculate tune out rate from this 
    if (tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt)/(1.0*row.start_total_impressions)))
        method=5
    elif (tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt+tsecond_qt)/(1.0*row.start_total_impressions)))
        method=6
    elif (tfirst_qt+tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt+tsecond_qt+tfirst_qt)/(1.0*row.start_total_impressions)))
        method=7
    else:
        r = np.array([ tfirst_qt, tsecond_qt, tthird_qt, tfull])
        c=1.0-np.max((r/row.start_total_impressions))
        tune_out = 100.0*(c)                  
        method=8 # the worst method    
    
  else:       
    # the best data 
    if (row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt)/(1.0*row.start_total_impressions)))
        method=1
    elif (row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt+row.second_qt)/(1.0*row.start_total_impressions)))
        method=2
    elif (row.first_qt+row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt+row.second_qt+row.first_qt)/(1.0*row.start_total_impressions)))
        method=3   
    else:    
        r = np.array([ row.first_qt, row.second_qt, row.third_qt, row.full])
        c=1.0-np.max((r/(1.0*row.start_total_impressions)))
        tune_out = 100.0*(c)                  
        method=4
  if np.isnan(tune_out) == True:
    tune_out=-1

  tune_out=int(100*tune_out)/100.
  return pd.Series({'method': method, 'tune_out': tune_out,  'showType':row.showType, 'ad_title':row.ad_title, \
                    'series_title':row.series_title, 'start_total_impressions':row.start_total_impressions, 'total_impressions':row.total_impressions, 'category_name':row.category_name, 'est_spend':row.est_spend, 'advertiser_name':row.advertiser_name })

#
# end of functions, beginning of script 
#

# apply the tune out formula to the pandas data frames from
# the csv files 
df=dataAds.apply(get_tune_out, axis=1)
df2=dataShow.apply(get_tune_out, axis=1)

# exclude method 4 and 8 because they are the worst methods 
df=df[(df.method != 4) & (df.method != 8)]
df2=df2[(df2.method != 4) & (df2.method != 8)]

# this is ordering by EST spend, which occasionally
# the customer wants (to prioritize those with high spend
# this section is for industry
unic=np.array(df2.category_name.unique())
ShowAds=[]
OtherAds=[]
EST=[]
for ic in unic:
    tp=np.nanmean(df2[df2.category_name == ic]['tune_out'])
    db=np.nanmean(df[df.category_name == ic]['tune_out'])
    print(str(ic) + ': '+str(tp) +', '+str(db))
    ShowAds.append(tp)
    OtherAds.append(db)
    EST.append(np.nansum(df2[df2.category_name == ic]['est_spend']))

# same, but this section is for brand    
ubrands=np.array(df2.advertiser_name.unique())
ShowAdsBrands=[]
OtherAdsBrands=[]
EST2=[]
for ib in ubrands:
    tp=np.nanmean(df2[df2.advertiser_name == ib]['tune_out'])
    db=np.nanmean(df[df.advertiser_name == ib]['tune_out'])
    print(str(ib) + ': '+str(tp) +', '+str(db))
    ShowAdsBrands.append(tp)
    OtherAdsBrands.append(db)
    EST2.append(np.nansum(df2[df2.advertiser_name==ib]['est_spend']))

# same, but this section is for ad title 
utitles=np.array(df2.ad_title.unique())
ShowAdsTitles=[]
OtherAdsTitles=[]
for ib in utitles:
    tp=np.nanmean(df2[df2.ad_title == ib]['tune_out'])
    db=np.nanmean(df[df.ad_title == ib]['tune_out'])
    print(str(ib) + ': '+str(tp) +', '+str(db))
    ShowAdsTitles.append(tp)
    OtherAdsTitles.append(db)
    
### 
# if needs to be ordered by estimated spend, this is the section
# to uncomment
###
#cats=[y for (x,y) in sorted(zip(EST, unic))][::-1]
#others=[y for (x,y) in sorted(zip(EST, Others))][::-1]
#tp=[y for (x,y) in sorted(zip(EST,ThisIsUs))][::-1]

# make them arrays
EST=np.array(EST)
unic=np.array(unic)
ShowAds = np.array(ShowAds); OtherAds = np.array(OtherAds)
ShowAdsBrands = np.array(ShowAdsBrands); OtherAdsBrands=np.array(OtherAdsBrands)
ShowAdsTitles=np.array(ShowAdsTitles); OtherAdsTitles=np.array(OtherAdsTitles)
EST2=np.array(EST2)

###
# another section to be uncommented if it needs to be ordered 
# by estimated spend. Specifically, Sean had requested
# only brands that had spent more than 400k
# this is adjustable
###
#diff=ThisIsUs2[EST2>400000]-Others2[EST2>400000]
#cats2=[y for (x,y) in sorted(zip(diff, ubrands[EST2>400000]))][::-1]
#others2=[y for (x,y) in sorted(zip(diff, Others2[EST2>400000]))][::-1]
#tp2=[y for (x,y) in sorted(zip(diff,ThisIsUs2[EST2>400000]))][::-1]

#
# the next 3 sections are order the values appropriately for the plots
# preparing the labels
# getting the 'over index' amount (difference)
#
ShowAds=ShowAds[OtherAds > 0]
OtherAds=OtherAds[OtherAds > 0]
diff1= ShowAds - OtherAds
cats=[y for (x,y) in sorted(zip(diff1, unic))][::-1]
cats = [ '\n'.join(wrap(l, 20)) for l in cats ]
others=[y for (x,y) in sorted(zip(diff1, OtherAds))][::-1]
tp=[y for (x,y) in sorted(zip(diff1,ShowAds))][::-1]
sDiff1=np.sort(diff1)[::-1]

OtherAdsBrands[np.isnan(OtherAdsBrands)==True]=ShowAdsBrands[np.isnan(OtherAdsBrands)==True]
diff=ShowAdsBrands-OtherAdsBrands
cats2=[y for (x,y) in sorted(zip(diff, ubrands))][::-1]
cats2 = [ '\n'.join(wrap(l, 20)) for l in cats2 ]
others2=[y for (x,y) in sorted(zip(diff, OtherAdsBrands))][::-1]
tp2=[y for (x,y) in sorted(zip(diff,ShowAdsBrands))][::-1]
sDiff2=np.sort(diff)[::-1]

OtherAdsTitles[np.isnan(OtherAdsTitles)==True]=ShowAdsTitles[np.isnan(OtherAdsTitles)==True]
diff3=ShowAdsTitles-OtherAdsTitles
cats3=[y for (x,y) in sorted(zip(diff3, utitles))][::-1]
cats3 = [ '\n'.join(wrap(l, 30)) for l in cats3 ]
others3=[y for (x,y) in sorted(zip(diff3, OtherAdsTitles))][::-1]
tp3=[y for (x,y) in sorted(zip(diff3,ShowAdsTitles))][::-1]
sDiff3=np.sort(diff3)[::-1]

#
# begin the plotting routine 
#

#
# first 
#
N=len(specIndustries)-1 # take the top 10. This number can be changed to be the top 8 for example

# first plot for Ad industry 
fig, ax = plt.subplots()
plt.subplots_adjust(bottom=0.4)
ind = np.arange(N)  
width = 0.35
rects1 = ax.bar(ind, tp[0:N], width, color='g', alpha=0.5)
rects2 = ax.bar(ind + width, others[0:N], width, color='b', alpha=0.5)
ax.set_ylabel('Attention Score')
ax.set_title('Attention by Ad Industry for ' + season + ' airings')
ax.set_xticks(ind + width)
ax.set_xticklabels([x.decode('UTF8') for x in cats[0:N]], rotation=80)
ax.set_ylim(50, 100)
autolabel(rects1,sDiff1[0:N])
ax.annotate('Over-Indexing', fontsize=14, xy=(150, -204),
            xycoords='axes pixels', xytext=(0, -200),
            textcoords='axes pixels')
ax.annotate('', fontsize=14, xy=(195, -195),
            xycoords='axes pixels', xytext=(145, -195),
            textcoords='axes pixels',
            arrowprops=dict(width = 5.,
                            headwidth = 15.,
                            frac = 0.2,
                            shrink = 0.05,
                            linewidth = 2,
                            color = 'green', alpha=0.5)
            )
# needs to be uncommented if we want est spend 
#ax.annotate('Est spend     ',
#            xy=(150, -200), xycoords='axes pixels',
#            xytext=(-130, 4), textcoords='offset pixels',  
#            arrowprops=dict(facecolor='green', alpha=0.5))
ax.legend((rects1[0], rects2[0]), (title, channelName+' '+daypart), loc='lower right')
plt.savefig(title+'_'+channelName+'_categories.png')


# second plot for Advertisers 
fig, ax = plt.subplots()
plt.subplots_adjust(bottom=0.4)
ind = np.arange(N)  
width = 0.35
rects1 = ax.bar(ind, tp2[0:N], width, color='g', alpha=0.5)
rects2 = ax.bar(ind + width, others2[0:N], width, color='b', alpha=0.5)
ax.set_ylabel('Attention Score')
ax.set_title('Attention by Advertiser for ' + season + ' airings')
ax.set_xticks(ind + width)
ax.set_xticklabels([x.decode('UTF8') for x in cats2[0:N]], rotation=80)
ax.set_ylim(50, 100)
autolabel(rects1,sDiff2[0:N])
ax.annotate('Over-Indexing', fontsize=14, xy=(150, -204),
            xycoords='axes pixels', xytext=(0, -200),
            textcoords='axes pixels')
ax.annotate('', fontsize=14, xy=(195, -195),
            xycoords='axes pixels', xytext=(145, -195),
            textcoords='axes pixels',
            arrowprops=dict(width = 5.,
                            headwidth = 15.,
                            frac = 0.2,
                            shrink = 0.05,
                            linewidth = 2,
                            color = 'green', alpha=0.5)
            )
ax.legend((rects1[0], rects2[0]), (title, channelName+' '+daypart), loc='lower right')
plt.savefig(title+'_'+channelName+'_brands.png')


# second plot for Ad titles 
fig, ax = plt.subplots()
plt.subplots_adjust(bottom=0.52)
ind = np.arange(N)  
width = 0.35
rects1 = ax.bar(ind, tp3[0:N], width, color='g', alpha=0.5)
rects2 = ax.bar(ind + width, others3[0:N], width, color='b', alpha=0.5)
ax.set_ylabel('Attention Score')
ax.set_title('Attention by Ad for ' + season + ' airings')
ax.set_xticks(ind + width)
ax.set_xticklabels([x.decode('UTF8') for x in cats3[0:N]], rotation=80)
ax.set_ylim(50, 100)
autolabel(rects1,sDiff3[0:N])
ax.annotate('Over-Indexing', fontsize=14, xy=(150, -304),
            xycoords='axes pixels', xytext=(0, -300),
            textcoords='axes pixels')
ax.annotate('', fontsize=14, xy=(195, -295),
            xycoords='axes pixels', xytext=(145, -295),
            textcoords='axes pixels',
            arrowprops=dict(width = 5.,
                            headwidth = 15.,
                            frac = 0.2,
                            shrink = 0.05,
                            linewidth = 2,
                            color = 'green', alpha=0.5)
            )
ax.legend((rects1[0], rects2[0]), (title, channelName+' '+daypart), loc='lower right')
plt.savefig(title+'_'+channelName+'_byadtitle.png')



