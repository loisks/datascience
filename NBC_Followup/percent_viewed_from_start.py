# percent_viewed_from_start.py
#
# see how many watch from start to end on average 
#
# LKS, June 2017
#
#
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import scipy.stats
import pandas as pd
import matplotlib
import datetime

DS=pd.read_csv('lois_nbc_tune_in_ad_after_start.tsv',delimiter='\t',names=['device_uuid', 'start','media_time', 'end', 'adid', 'occurrence_id'] )
DS['start']=pd.to_datetime(DS['start'])
DS['end']=pd.to_datetime(DS['end'])

totals=[]
uniOccur=DS.occurrence_id.unique()
for uni in uniOccur:
  try:
    DK=DS[DS.occurrence_id ==uni]
    if len(DK) > 10:
       DK['time_diff']=DK['end']-DK['start']
       DK['time_diff']=DK['time_diff'].dt.total_seconds()
       
       DK['second_start']=(DK['start']-datetime.datetime(2017,6,4)).dt.total_seconds()
       DK['second_end']=(DK['end']-datetime.datetime(2017,6,4)).dt.total_seconds()

       
       cutoffss=float(DK['second_start'].mode())
       cutoffse=float(DK['second_end'].mode())
       # 2 is for a buffer
       DF=DK[(DK['second_start'] <= cutoffss+2.0) & (DK['second_start'] >= cutoffss-2.0) & (DK['second_end'] <= cutoffse+2.0) & (DK['second_end'] >= cutoffse-2.0)]
       oDF=DK[ (DK['second_start'] > cutoffss+2.0)  & (DK['second_end'] <= cutoffse+2.0) & (DK['second_end'] >= cutoffse-2.0)]
       if len(DF) > 10:
           temp=len(oDF)/(1.0*len(oDF)+1.0*len(DF))
           #cutoff=DF['time_diff'].median()-2.0
           #temp=len(DF[DF['time_diff']< cutoff])/(1.0*len(DF))
           print(temp)
           totals.append(temp)
  except KeyboardInterrupt:
      raise
  except:
      print('pass')
      pass
import pickle
pickle.dump(totals, open('total_from_start.p', 'wb'))
