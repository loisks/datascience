# NBC_viewrate_comparison.py
#
# compare average view rate and percent viewed
#
# LKS, June 2017
#
#
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import scipy.stats
import pandas as pd
import matplotlib

DS=pd.read_csv('lois_with95percentile_v2.csv',header=0)

new_pv=pd.to_numeric(DS['avg_new_pv'], errors='coerce')
old_pv=pd.to_numeric(DS['avg_old_pv'], errors='coerce')


os.chdir('NBC_Followup')
fig=plt.figure()
ax=fig.add_subplot(111)
ax.hist(np.array(new_pv.dropna()), bins=np.linspace(60,100,41), color='green', alpha=0.5)
ax.set_ylabel('Number of Occurrences')
ax.set_xlabel('Attention  (%)')
plt.savefig('attention_hist.pdf')


fig=plt.figure()
ax=fig.add_subplot(111)
ax.hist(np.array(old_pv.dropna()), bins=np.linspace(60,100,41), color='green', alpha=0.5)
ax.set_ylabel('Number of Occurrences')
ax.set_xlabel('Average View Rate  (%)')
plt.savefig('average_view_rate_hist.pdf')
os.chdir('..')
