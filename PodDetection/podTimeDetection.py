# podTimeDetection.py
#
# try to develop an algorith that predicts where pods are located in
# a show
#
# LKS January 2017
#

import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
import datetime
from scipy import stats

def td(ts):
    if ts > 10000:
        ts= ts-3600*3 # it's on pacific time
    return ts

show='TheVoice'
df=pd.read_csv(show+'Times.csv', delimiter=';')

#
# convert the occurrence times and show start time to python
# datetimes
df['showStart']=pd.to_datetime(df['showStart'])
df['occurrencedatetime_est']=pd.to_datetime(df['occurrencedatetime_est'])

df['timeDiff']=(df.occurrencedatetime_est-df.showStart).astype('timedelta64[s]')
df['timeDiff']=df['timeDiff'].apply(td)
df=df[(df['timeDiff']< 3500) & (df['timeDiff']>0)] # nothing for the start of the next show
#
# now let's order by program airing id
uniPs=df['program_airing_id'].unique()
podMax=df.podNumber.max()

data = pd.DataFrame([])

for ipod in range(1,podMax+1):
 for iuni in uniPs:
    # get pod 1
    try:
       temp=df[(df['program_airing_id']== iuni) & (df['podNumber']==ipod)]
       
       start=min(np.array(temp['timeDiff']))
       cend=float(np.array(temp[temp['spot']==max(temp['spot'])]['duration'])[0])

       end=max(np.array(temp['timeDiff']))+cend
       data = data.append(pd.DataFrame({'podNo': ipod, 'Start': start, 'End': end, 'Duration': end-start}, index=[0]), ignore_index=True)
    except:
        None
        # now get the 

def confidenceInterval(a):
    mean, sigma = np.mean(a), np.std(a)
    conf_int = stats.norm.interval(0.99, loc=mean, scale=sigma / np.sqrt(len(a)))
    return(conf_int)

startCI=[]
endCI=[]
durationByPod=[]
stdByPod=[]

data=data[(data['Duration'] < data.Duration.mean() + data.Duration.std())]
data=data[(data['Duration'] > data.Duration.mean() - data.Duration.std())]

for ipod2 in range(1,6):
   startCI.append(confidenceInterval(np.array(data[data['podNo']==ipod2]['Start'])))
   endCI.append(confidenceInterval(np.array(data[data['podNo']==ipod2]['End'])))
   durationByPod.append(data[data['podNo']==ipod2]['Duration'].mean())
   stdByPod.append(data[data['podNo']==ipod2]['Duration'].std())
   
print(startCI)
print(endCI)
print(durationByPod)
print(stdByPod)
