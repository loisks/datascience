# productionScript.py
#
# pod detection, set up cascade so it makes sense 
#
# LKS, January 2017
#
#

import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
import datetime
from scipy import stats



def td(ts):
    if ts > 10000:
        ts= ts-3600*3 # it's on pacific time
    return ts

def wrongPod(pod):
    pod=pod-1
    return( pod)

df0=pd.read_csv('4weeksData.csv', delimiter=';')
uniTitles=df0.title.unique()
finalDF=[]
for title in uniTitles:
  try:
     # This section basically puts the a show's time duration into a number of seconds
     df=df0[df0.title == title] # get the right data 
     df['showStart']=pd.to_datetime(df['showStart'])
     df['occurenceDateTime']=pd.to_datetime(df['occurenceDateTime'])
     
     df['timeDiff']=(df.occurenceDateTime-df.showStart).astype('timedelta64[s]')
     df['timeDiff']=df['timeDiff'].apply(td)
     df=df[(df['timeDiff']< 3500) & (df['timeDiff']>0)] # nothing for the start of the next show
     #
     # now let's order by program airing id
     note=df[df.timeDiff < 20]['program_airing_id']
     mask = df['program_airing_id'].isin(np.array(note))
     df=df[~mask]
     uniPs0=df['program_airing_id'].unique()
     
     podMeanMax=[]
     for iP in uniPs0:
         podMeanMax.append(df[df.program_airing_id == iP].pod.max())
         
     podMax=int(np.nanmean(podMeanMax)  )  
     #
     # only base the pod off of the right number of pods
     uniPs=df[df.pod==podMax]['program_airing_id'].unique()
     
     data = pd.DataFrame([])
     
     for ipod in range(1,podMax+1):
      for iuni in uniPs:
         # get pod 1
         try:
            temp=df[(df['program_airing_id']== iuni) & (df['pod']==ipod)]
            
            start=min(np.array(temp['timeDiff']))
            cend=float(np.array(temp[temp['spot']==max(temp['spot'])]['Duration'])[0])
     
            end=max(np.array(temp['timeDiff']))+cend
            data = data.append(pd.DataFrame({'podNo': ipod, 'Start': start, 'End': end, 'Duration': end-start}, index=[0]), ignore_index=True)
         except:
             None
             # now get the 
     
         
     def confidenceInterval(a):
         mean, sigma = np.mean(a), np.std(a)
         conf_int = stats.norm.interval(0.99, loc=mean, scale=sigma / np.sqrt(len(a)))
         return(conf_int)
     
     startCI=[]
     endCI=[]
     durationByPod=[]
     stdByPod=[]
     
     data=data[(data['Duration'] < data.Duration.mean() + data.Duration.std())]
     data=data[(data['Duration'] > data.Duration.mean() - data.Duration.std())]
     if len(data[data.podNo== 1]) ==0:
         print(title)
         data['podNo']=data['podNo'].apply(wrongPod)
         podMax-=1
     
     for ipod2 in range(1,podMax):
        startCI.append(confidenceInterval(np.array(data[data['podNo']==ipod2]['Start'])))
        endCI.append(confidenceInterval(np.array(data[data['podNo']==ipod2]['End'])))
        durationByPod.append(data[data['podNo']==ipod2]['Duration'].mean())
        stdByPod.append(data[data['podNo']==ipod2]['Duration'].std())
        finalDF.append({'series': df.seriesid.values[0], 'channel': df.channel.values[0], 'podNum':ipod2, 'start_min':confidenceInterval(np.array(data[data['podNo']==ipod2]['Start']))[0], 'start_max':confidenceInterval(np.array(data[data['podNo']==ipod2]['Start']))[1],
                        'end_min':confidenceInterval(np.array(data[data['podNo']==ipod2]['End']))[0],
                        'end_max':confidenceInterval(np.array(data[data['podNo']==ipod2]['End']))[1],
                        'duration': data[data['podNo']==ipod2]['Duration'].mean(), 'duration_std':data[data['podNo']==ipod2]['Duration'].std()})
     try:
         print(title)  
         print([int(100*x[0])/100. for x in startCI])
         print([int(x[1]*100)/100. for x in startCI])
         print([int(100*x[0])/100. for x in endCI])
         print([int(x[1]*100)/100. for x in endCI])
         print([int(x*100)/100. for x in durationByPod])
         print([int(x*100)/100. for x in stdByPod])
     except:
         print('bad pod in '+title)
  except:
      print('shitty!')
         
final=pd.DataFrame(finalDF)
final.to_csv('NBC_podConfidenceIntervals.csv', delimiter=';')


# test!
#
# calculate standard deviation
def stddev(lst):
    """returns the standard deviation of lst"""
    mn = mean(lst)
    variance = sum([(e-mn)**2 for e in lst]) / len(lst)
    return sqrt(variance)
