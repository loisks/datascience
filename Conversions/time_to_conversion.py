# time_to_conversion.py
#
# figure out how long it takes to get a conversion on average
#
# LKS, June 2017
#
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import seaborn as sns
import scipy.stats
import pandas as pd
import matplotlib
import json
#
# run redshift queries locally
from sqlalchemy import create_engine

def create_db_engine(db_type, host, port, database, user, password):
    return create_engine(db_type + '://' + user + ':' + password + '@' + host + ':' + port + '/' + database)


def create_db_engine_with_credentials(db_cred_file_name, conn_name):
    with open(db_cred_file_name) as json_file:
        db_credentials = json.load(json_file)
        conn = db_credentials[conn_name]
        # print type(conn['port'])
        return create_db_engine(conn['db_type'], conn['host'], conn['port'], conn['database'],
                                conn['user'], conn['password'])

redshift_engine = create_db_engine_with_credentials('/audience-core/credentials/db_credentials.json', 'ispot_redshift')

# get the unique advertiser_id

QueryPre=\
          """ Select distinct(site_id) site_id FROM public.conversion_by_impression;"""
alist= pd.read_sql_query(QueryPre, con=redshift_engine, coerce_float=False)
alist=alist[alist.site_id != 'TC-1002-1']
alist=alist[alist.site_id != 'TC-1002-2']

#appList=[]
#deviceList=[]
#adList=[]
#for pp in range(len(alist)):
#    item=str(alist.iloc[pp]['site_id'])
#    Query2=\
#           """           
#         SELECT imp.device_uuid, min( EXTRACT(epoch from (conversion_time - impression_time)) ) time_diff, ad_id 
#        FROM public.conversion_by_impression imp
#        LEFT OUTER JOIN public.device_blacklist black on imp.occurrence_id = black.occurrence_id
#        WHERE site_id = '{0}'
#        GROUP BY imp.device_uuid, ad_id
#        HAVING min( EXTRACT(epoch from (conversion_time - impression_time)) ) <= (86400) ;
#        --AND  min( EXTRACT(epoch from (conversion_time - impression_time)) ) > 30;""".format(item)
#    f7 = pd.read_sql_query(Query2, con=redshift_engine, coerce_float=False)
#    print(f7)
#    appList.extend(list(np.array(f7['time_diff'])))
#    deviceList.extend(list(np.array(f7['device_uuid'])))
#    adList.extend(list(np.array(f7['ad_id'])))
#df7=pd.DataFrame(np.column_stack([appList, deviceList, adList]), columns=['time', 'device', 'adid'])
#df7.time=pd.to_numeric(df7.time)
#df7.to_csv('allconversions_24hour.csv', index=False)

blacklistAds=[u'1435476', u'1467373', u'1471240', u'1471243', u'1472088', u'1477696',
       u'1516630', u'1516826', u'1516869', u'1517033', u'1518712']



df7=pd.read_csv('allconversions_24hour.csv')
df7=df7[df7.adid.isin(blacklistAds) == False]

fig=plt.figure()
ax=fig.add_subplot(111)
df7.time.hist(bins=70)
ax.set_xlabel('Seconds From Impression')
ax.set_ylabel('Counts')
ax.set_title('Time From Ad Airing to Conversion')
plt.savefig('avgtimetoconv_24hour2.pdf')

fig=plt.figure()
ax=fig.add_subplot(111)
df7.time.hist(bins=np.linspace(0, 300, 61))
ax.set_xlabel('Seconds From Impression')
ax.set_ylabel('Counts')
ax.set_title('Time From Ad Airing to Conversion')
plt.savefig('avgtimetoconv_2minutes.pdf')


#
# now get occurrence id stuff 
#

appList=[]
deviceList=[]
adList=[]
occList=[]
for pp in range(len(alist)):
    item=str(alist.iloc[pp]['site_id'])
    Query3=\
           """  
         SELECT imp2.occurrence_id, temp.device_uuid, temp.time_diff, temp.ad_id
         FROM (SELECT imp.device_uuid, min( EXTRACT(epoch from (imp.conversion_time - imp.impression_time)) ) time_diff, ad_id 
        FROM public.conversion_by_impression imp
        LEFT OUTER JOIN public.device_blacklist black on imp.occurrence_id = black.occurrence_id
        WHERE site_id = '{0}'
        GROUP BY imp.device_uuid, ad_id
        HAVING min( EXTRACT(epoch from (conversion_time - impression_time)) ) <= (600)
        AND min( EXTRACT(epoch from (conversion_time - impression_time)) ) > 0) temp 
        JOIN public.conversion_by_impression imp2 on temp.time_diff = EXTRACT(epoch from (imp2.conversion_time - imp2.impression_time)) and temp.ad_id = imp2.ad_id and temp.device_uuid = imp2.device_uuid        
        ;""".format(item)
    f7 = pd.read_sql_query(Query3, con=redshift_engine, coerce_float=False)
    print(f7)
    appList.extend(list(np.array(f7['time_diff'])))
    deviceList.extend(list(np.array(f7['device_uuid'])))
    adList.extend(list(np.array(f7['ad_id'])))
    occList.extend(list(np.array(f7['occurrence_id'])))
df7=pd.DataFrame(np.column_stack([appList, deviceList, adList, occList]), columns=['time', 'device', 'adid', 'occurrence_id'])
df7.time=pd.to_numeric(df7.time)
df7.to_csv('allconversions_24hour_occ.csv', index=False)

blacklistAds=[u'1435476', u'1467373', u'1471240', u'1471243', u'1472088', u'1477696',
       u'1516630', u'1516826', u'1516869', u'1517033', u'1518712']

df7=df7[df7.adid.isin(blacklistAds) == False]
