# tuneOutDist.py
#
# tune out rate distribution across a week of NBC show data
#
# LKS, February 2017
#

import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
from scipy.stats import ttest_ind
from scipy.stats import pearsonr
import matplotlib.dates as mdates
import seaborn as sns
data2=pd.read_csv('superbowl.csv', sep=';', header=0)

def get_tune_out(row):
  #
  # input
  # row = row of pandas dataframe
  #
  # output
  # tuneout rate row and other things
  #
  
  if row.start == 0:
    # redistribute the wealth
    #tstart = (.7*row.first_qt)+(.2*row.second_qt)+(.01*row.third_qt)+\
    #         (.01*row.full)
    #tfirst_qt=row.first_qt- .7*row.first_qt
    #tsecond_qt=row.second_qt-.2*row.second_qt
    #tthird_qt=row.third_qt- .01*row.third_qt
    #tfull =row.full- .01*row.full
    tstart = (.8*row.first_qt)+(.3*row.second_qt)+(.01*row.third_qt)+\
             (.01*row.full)
    tfirst_qt=row.first_qt- .8*row.first_qt
    tsecond_qt=row.second_qt-.3*row.second_qt
    tthird_qt=row.third_qt- .01*row.third_qt
    tfull =row.full- .01*row.full
    
    # calculate tune out rate from this
    try:
     if (tthird_qt+tfull > 0.8*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt)/(1.0*row.start_total_impressions)))
        method=5
     elif (tsecond_qt+tthird_qt+tfull > 0.8*row.start_total_impressions) :
        temp=0.15*row.full
        tthird_qt += 0.12*row.full
        if tfull+tthird_qt+tsecond_qt > row.start_total_impressions:
          tthird_qt -= temp
        tune_out = 100.0 * (((tfull+tthird_qt+tsecond_qt)/(1.0*row.start_total_impressions)))
        method=6
     elif (row.start_total_impressions != 0) & (0.6*tstart / (1.0*row.start_total_impressions) < 0.2):
      tstart=0.6*tstart
      tune_out = 100*(1.0+0.02-(tstart/ (1.0*row.start_total_impressions)))
      method=7
     else:
      tune_out = 100.0*((row.start_total_impressions/(1.0*row.total_impressions)))
      method=8

#     elif (tfirst_qt+tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
#        tune_out = 100.0 * (((tfull+tthird_qt+tsecond_qt+tfirst_qt)/(1.0*row.start_total_impressions)))
#        method=7
#     else:
#        r = np.array([ tfirst_qt, tsecond_qt, tthird_qt, tfull])
#        c=1.0-np.max((r/row.start_total_impressions))
#        tune_out = 100.0*(c)                  
#        method=8 # the worst method
    except(ZeroDivisionError):
       # r = np.array([ row.first_qt, row.second_qt, row.third_qt, row.full])
       # c=1.0-np.max((r/(1.0*row.start_total_impressions)))
       # tune_out = 100.0*(c)                  
       # method=8
      tune_out = np.nan    
      method=9
    if (row.start_total_impressions < 20) or (tune_out < 80):
      tune_out= np.nan
      method=9
    
  else:
    try:
    # the best data 
     if (row.third_qt+row.full > 0.8*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt)/(1.0*row.start_total_impressions)))
        method=1
     elif (row.second_qt+row.third_qt+row.full > 0.8*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt+row.second_qt)/(1.0*row.start_total_impressions)))
        method=2
     elif (row.start_total_impressions != 0) & (row.start !=0) & (row.start / (1.0*row.start_total_impressions) < 0.2):
       tune_out = 100*(1.0-(row.start / (1.0*row.start_total_impressions)))
       method=3
     else:
      tune_out = 100.0*(1.0-(row.start_total_impressions/(1.0*row.total_impressions)))     
      method=4
  
#     elif (row.first_qt+row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
#        tune_out = 100.0 * (((row.full+row.third_qt+row.second_qt+row.first_qt)/(1.0*row.start_total_impressions)))
#        method=3   
#     else:    
#        r = np.array([ row.first_qt, row.second_qt, row.third_qt, row.full])
#        c=1.0-np.max((r/(1.0*row.start_total_impressions)))
#        tune_out = 100.0*(c)                  
#        method=4
    except(ZeroDivisionError):
       #r = np.array([ row.first_qt, row.second_qt, row.third_qt, row.full])
       #c=1.0-np.max((r/(1.0*row.start_total_impressions)))
       #tune_out = 100.0*(c)                  
       #method=4
      tune_out = np.nan   
      method=9
    if (row.start_total_impressions < 20) or (tune_out < 80):
      tune_out= np.nan
      method=9
#    
  if np.isnan(tune_out) == True:
    tune_out=-1

  tune_out=100.0-int(100*tune_out)/100.
  return pd.Series({'method': method, 'lois_tune_out': tune_out,  'start_total_impressions':row.start_total_impressions,\
                    'total_impressions':row.total_impressions, 'ad_title':row.ad_title, 'ad_id':row.AdID, \
                    'first_qt':row.first_qt, 'second_qt':row.second_qt, 'start':row.start, 'third_qt':row.third_qt, 'full':row.full})

#data2=data2[data2.spot_reach != 'S']
cdf=data2
df=cdf.apply(get_tune_out, axis=1)
print(len(df[df.method==9]))
print('percentage:')
print(len(df[df.method==9])/(1.0*len(df)))
df=df[df.lois_tune_out>0]
#fig=plt.figure()
#ax=fig.add_subplot(111)
#plt.title('Tune Out Rates for NBC from 2-2-2017 to 2-8-2017')
##sns.distplot(df['tune_out'], df['AverageViewRate'])
#sns.regplot(x='tune_out', y='AverageViewRate', data=df[df['method']==1], color='b')
#sns.regplot(x='tune_out', y='AverageViewRate', data=df[df['method']==2], color='r')
#sns.regplot(x='tune_out', y='AverageViewRate', data=df[df['method']==3], color='g')
#sns.regplot(x='tune_out', y='AverageViewRate', data=df[df['method']==4], color='y')
#plt.show()
#
#
#fig=plt.figure()
#ax=fig.add_subplot(111)
#plt.title('Tune Out Rates for NBC from 2-2-2017 to 2-8-2017')
##sns.distplot(df['tune_out'][df['tune_out']>0], df['AverageViewRate'][df['tune_out']>0])
#sns.regplot(x='tune_out', y='AverageViewRate', data=df[df['method']==5], color='b')
#sns.regplot(x='tune_out', y='AverageViewRate', data=df[df['method']==6], color='r')
#sns.regplot(x='tune_out', y='AverageViewRate', data=df[df['method']==8], color='g')
##sns.regplot(x='tune_out', y='AverageViewRate', data=df[df['method']==8], color='y')
#plt.show()
#
#fig=plt.figure()
#ax=fig.add_subplot(111)
#plt.title('Tune Out Rates Method =3  for NBC from 2-2-2017 to 2-8-2017')
#sns.jointplot(x='tune_out', y='AverageViewRate', data=df[(df.method==3) & (df.AverageViewRate)], kind='kde')
#plt.show()
#
#fig=plt.figure()
#ax=fig.add_subplot(111)
#plt.title('Tune Out Rates Method = 4 for NBC from 2-2-2017 to 2-8-2017')
#sns.jointplot(x='tune_out', y='AverageViewRate', data=df[(df.method==4) & (df.AverageViewRate)], kind='kde')
#plt.show()
#
#fig=plt.figure()
#ax=fig.add_subplot(111)
#plt.title('Tune Out Rates Method = 8 for NBC from 2-2-2017 to 2-8-2017')
#sns.jointplot(x='tune_out', y='AverageViewRate', data=df[(df.method==8) & (df.AverageViewRate)], kind='kde')
#plt.show()
#
ewan_sb=pd.read_csv('ewan_sb.csv')
merged=df[['ad_id', 'lois_tune_out']].merge(ewan_sb,on='ad_id',how='outer')
merged.head()
merged.to_csv('updated_tune_out.csv')




fig=plt.figure()
ax=fig.add_subplot(111)
ax = sns.distplot(df[(df.method==1)]['lois_tune_out'], rug=True, rug_kws={"color": "b"},
                  hist_kws={"histtype": "step", "linewidth": 3,
                            "alpha": 0.5, "color": "b"}, label='Method 1')
ax = sns.distplot(df[ (df.method==2)]['lois_tune_out'], rug=True, rug_kws={"color": "r"},
                  hist_kws={"histtype": "step", "linewidth": 3,
                            "alpha": 0.5, "color": "r"}, label='Method 2')
ax = sns.distplot(df[ (df.method==3)]['lois_tune_out'], rug=True, rug_kws={"color": "g"},
                  hist_kws={"histtype": "step", "linewidth": 3,
                            "alpha": 0.5, "color": "g"}, label='Method 3')
#ax = sns.distplot(df[ (df.method==4)]['tune_out'], rug=True, rug_kws={"color": "y"},
#                  hist_kws={"histtype": "step", "linewidth": 3,
#                            "alpha": 0.5, "color": "y"}, label='Method 4')
ax.set_xlim(50,100)
ax.set_ylim(0, 0.15)
plt.legend( loc='upper left')
plt.show()


