# feedOffset.py
#
# get the feed offset and plot it
#
# LKS, March 2017
#
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
import matplotlib.dates as mdates
import pickle
import scipy.stats as stats

# get the feed offset plots

data=pd.read_csv('feedOffset.csv', sep=';', header=0)

fig=plt.figure()
plt.hist(data.feed_offset,30, range=[-50,50])
plt.savefig('overallHist.png')

#data.feed_offset[data.feed_offset>10]=10
#data.feed_offset[data.feed_offset<-10]=-10


os.chdir('ByChannel')
f = open('byChannelResults.txt','wb')
csv=[ np.zeros(6) for ij in range(len(data.channel.unique()))]
count=0
for i in np.sort(data.channel.unique()):
   
   #fig=plt.figure()
   #ax=fig.add_subplot(111)
   #plt.title('Channel = '+ str(i))
   #ax.text(0.9, 0.9, 'PST Earlier',
   #     verticalalignment='bottom', horizontalalignment='right',
   #     transform=ax.transAxes,
   #      fontsize=15)
   #ax.text(0.3, 0.9, 'EST Earlier',
   #     verticalalignment='bottom', horizontalalignment='right',
   #     transform=ax.transAxes,
   #      fontsize=15)
   #ax.set_xlim(-50,50)
   #ax.hist(np.array(data[data.channel==i].feed_offset),30, range=[-50,50])
   #plt.savefig('channel='+str(i)+'_big.png')
   fo=np.array(data[data.channel==i].feed_offset)
   #f.write('Channel: '+str(i) +', mean is: '+str(np.round(100.0*np.nanmean(fo))/100.) + ', stddev is: '+str(np.round(100*np.nanstd(fo))/100.)+' \n')
   mean, sigma = fo.mean(), fo.std()
   conf = stats.norm.interval(0.99, loc=mean, scale=sigma)
   csv[count][0]=i
   csv[count][1]=np.round(100*conf[0])/100.
   csv[count][2]=np.round(100*conf[1])/100.
   csv[count][3]=np.round(100*sigma)/100.
   csv[count][4]=len(fo)
   csv[count][5]=len(np.where((fo < conf[0]) | (fo > conf[1]))[0])
   fig=plt.figure()
   ax=fig.add_subplot(111)
   plt.title('Channel = '+ str(i) + ', mean is: '+ str(np.nanmean(fo))+' and stddev is: '+str( np.nanstd(fo)))
   ax.set_xlim(-10,10)
   ax.text(0.9, 0.9, 'PST Earlier',
        verticalalignment='bottom', horizontalalignment='right',
        transform=ax.transAxes,
         fontsize=15)
   ax.text(0.3, 0.9, 'EST Earlier',
        verticalalignment='bottom', horizontalalignment='right',
        transform=ax.transAxes,
         fontsize=15)
   ax.hist(fo,21, range=[-10,10])
   plt.savefig('channel='+str(i)+'_small.png')
   count+=1

csvArr=np.array(csv)
np.savetxt('7monthsResults.csv', csvArr, delimiter=',')
os.chdir('..')   

# CI for all data by series

mean, sigma = data.feed_offset.mean(), data.feed_offset.std()
conf = stats.norm.interval(0.99, loc=mean, scale=sigma)

rogueSeries=data[(data.feed_offset > conf[1]) | (data.feed_offset < conf[0])].series
listRS=rogueSeries.unique()
#np.savetxt('RogueSeries.txt',[str(i) for i in list(listRS)], delimiter=',')
with open('RogueSeries.txt', 'wb') as f:
    for i in listRS:
        f.write(str(i))
        f.write('\n')

#flags=[]
#for i in data.series.unique():
#    # find if the series is an anamoly for it's channel
#    fo1=data[data.series==i]
#    
#    fo=np.array(fo1.feed_offset)
#
#    col=np.swapaxes(csv, 1, 0)
#    tt=np.where(col[0] == list(chan)[0])[0][0]
#    tcol=csv[tt]
#    if (np.nanmean(fo) < .7*tcol[1]) & (np.nanmean(fo) > .7*tcol[2]):
#        print('flagged series: '+ i)
#        flags.append(i)
#    print('lower limit: '+str(tcol[1]) + ' upper limit: '+ str(tcol[2]))
#    print('mean is: '+str(np.nanmean(fo)))
#




#   #fig=plt.figure()
#   #ax=fig.add_subplot(111)
#   #plt.title('Series = '+ str(i))
#   #ax.text(0.9, 0.9, 'PST Earlier',
#   #     verticalalignment='bottom', horizontalalignment='right',
#   #     transform=ax.transAxes,
#   #      fontsize=15)
#   #ax.text(0.3, 0.9, 'EST Earlier',
#   #     verticalalignment='bottom', horizontalalignment='right',
#   #     transform=ax.transAxes,
#   #      fontsize=15)
#   #ax.set_xlim(-50,50)
#   #ax.hist(np.array(data[data.series==i].feed_offset),30, range=[-50,50])
#   #plt.savefig('channel='+str(i)+'_big.png')
#
#   fig=plt.figure()
#   ax=fig.add_subplot(111)
#   plt.title('Series = '+ str(i))
#   ax.set_xlim(-10,10)
#   ax.text(0.9, 0.9, 'PST Earlier',
#        verticalalignment='bottom', horizontalalignment='right',
#        transform=ax.transAxes,
#         fontsize=15)
#   ax.text(0.3, 0.9, 'EST Earlier',
#        verticalalignment='bottom', horizontalalignment='right',
#        transform=ax.transAxes,
#         fontsize=15)
#   ax.hist(np.array(data[data.series==i].feed_offset),21, range=[-10,10])
#   plt.savefig('channel='+str(i)+'_small.png')
#   
#os.chdir('..')


#
# case study tests 
#
# run date test by month by week for NBC
#data2=data
#data2['east_start']=pd.to_datetime(data2['east_start'])
#dateThreshu=['20160901', '20161201', '20170301']
#dateThreshl=['20160801', '20161101', '20170201']
#csv=[ np.zeros(6) for ij in range(len(dateThreshu))]
#count=0
#for item in range(len(dateThreshu)):
#   datat=data2[(data2.east_start < pd.to_datetime(dateThreshu[item])) & (data2.east_start > pd.to_datetime(dateThreshl[item]))]
#   fo=np.array(datat[datat.channel==105].feed_offset)
#   
#   #f.write('Channel: '+str(i) +', mean is: '+str(np.round(100.0*np.nanmean(fo))/100.) + ', stddev is: '+str(np.round(100*np.nanstd(fo))/100.)+' \n')
#   
#   mean, sigma = fo.mean(), fo.std()
#   conf = stats.norm.interval(0.99, loc=mean, scale=sigma)
#   csv[count][0]=105
#   csv[count][1]=np.round(100*conf[0])/100.
#   csv[count][2]=np.round(100*conf[1])/100.
#   csv[count][3]=np.round(100*sigma)/100.
#   csv[count][4]=len(fo)
#   csv[count][5]=len(np.where((fo < conf[0]) | (fo > conf[1]))[0])
#   count+=1
