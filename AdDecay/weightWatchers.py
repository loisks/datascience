# weightWatchers.py
#
# test two ads, see what's up
#
# LKS May 2017
#

import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import scipy.stats
import pandas as pd
import matplotlib
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import roc_auc_score, r2_score
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.cross_validation import train_test_split

ad1=1452953
ad2=1461755


# load in the data file
labels=["occurrence_id", "advertiser_id", "duration_sec", "pod_letter", "seriesid", "show_type", "channel_id", "avg_new_pv", "occurrence_is_fta", "OccurenceDateTime", "first_airing_date", "date_est", "attribution_window", "conversions", "unique_devices","ad_id", "day_part_id"]


DS=pd.read_csv('lois_nbc_2017_output_v5.csv', names=labels)
DS1=DS[DS['ad_id']==ad1]
DS2=DS[DS['ad_id']==ad2]
