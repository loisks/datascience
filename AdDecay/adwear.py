# adwear.py
#
# calculate the wear on an ad 
#
# LKS, May 2017
#

#
# put ad decay for a quarter
# variability of model
# [CHECK]loss per second on duration plot instead of average percent viewed 
#
# tomorrow first thing
# literally this hasn't changed 
# (1) Seans' ad wear weight watchers thing
# (2) continue working on model, ad in new data set 
# 
#
#

import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import seaborn as sns
import scipy.stats
import pandas as pd
import matplotlib
import datetime
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import roc_auc_score, r2_score
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.cross_validation import train_test_split
from copy import deepcopy

# labels
labels=['day_part_id','adid','advertiserid','duration_sec','pod_letter','seriesid','show_type','channel_id','avg_new_pv','occurrence_is_fta','occurrence_id','occurrence_airing_date','first_airing_date','unique_devices','conversions','impression_time', 'conversion_time', 'site_id', 'conversion_type']

DS=pd.read_csv('lois_nbc_output_2017_v12.csv', names=labels)


DF=DS[['day_part_id','adid','advertiserid','duration_sec','pod_letter','seriesid','show_type','channel_id','avg_new_pv','occurrence_is_fta','occurrence_id','occurrence_airing_date','first_airing_date']]

DF['first_airing_date']=pd.to_datetime(DF['first_airing_date'])
DF=DF[DF['first_airing_date'] >= datetime.datetime.strptime('2016-12-26', '%Y-%m-%d')]

DF2=DF.groupby('occurrence_id').mean().join(pd.DataFrame(DF.groupby('occurrence_id').size(), 
                                columns=['counts']))
DF2=DF2[DF2.counts > 5]
# sort by occurrenceid

adList=DF2.adid.unique()
ad1=adList[1]


window=15
ini_window=25
hcount=0; wcount=0; ccount=0
for aditem in adList:
    ads=DF2[DF2.adid==aditem]
    ads=ads[np.isnan(ads.avg_new_pv) == False]
    if len(ads) > ini_window:
        
        rollingmed=pd.rolling_median(ads, window)
        inimed=pd.rolling_median(ads,ini_window)
        #
        # look for a 3% change?
        warning=rollingmed[rollingmed.avg_new_pv< 0.93*inimed.avg_new_pv.iloc[[ini_window]].values[0]]
        heavywear=rollingmed[rollingmed.avg_new_pv< 0.88*inimed.avg_new_pv.iloc[[ini_window]].values[0]]
        print(rollingmed)
        
        if len(heavywear) > 0:
            print('heavy wear!')
            hcount+=1
        elif len(warning) > 0:
            print('warning!')
            wcount+=1
        else:
            print('in the clear!')
            ccount+=1


        # MAKE A PLOT FOR EACH ONE
        fig=plt.figure()
        ax=fig.add_subplot(111)
        ads['place']=range(len(ads.index))
        rollingmed['place']=range(len(rollingmed.index))
        ax.scatter(ads.place, ads.avg_new_pv, marker='x', color='k')
        ax.scatter(rollingmed.place, rollingmed.avg_new_pv, marker='o', color='green', alpha=0.5)
        try:
            ax.scatter(rollingmed[rollingmed.index==warning.index[0]].place, 100, marker='^', color='yellow', s=60)
        except:
            None
        try:
            ax.scatter(rollingmed[rollingmed.index==heavywear.index[0]].place, 100, marker='^', color='red', s=60)
        except:
            None
        ax.set_ylabel('Average Percent Viewed')
        ax.set_xlim(0,max(ads['place']))
        ax.set_xlabel('Ad Occurrences')
        #ax.set_xticklabels()
        plt.savefig('adid='+str(aditem)+'_adwear.pdf')

stop




# look at a few campaigns






stop
#DF2=DF[np.isnan(DF["conversions"])==False]

DF2=DF2.join(DF2.groupby('advertiserid')['unique_devices'].sum(), on='advertiserid', rsuffix='_by_advertiserid')
#DF2=DF2[(DF2.unique_devices_by_adid > 100000) & (DF2.unique_devices_by_adid < 500000)]
DF2['convpct']=DF2["conversions"]/DF2["unique_devices_by_advertiserid"]
DF2=DF2[(DF2.convpct < 0.5) & (DF2.convpct > 0.015)]
results=DF2[['avg_new_pv', 'convpct', 'advertiserid']].groupby('advertiserid').mean()

#
# 
#DF2=DF2.join(DF2.groupby('adid')['unique_devices'].sum(), on='adid', rsuffix='_by_adid')
##DF2=DF2[(DF2.unique_devices_by_adid > 100000) & (DF2.unique_devices_by_adid < 500000)]
#DF2['convpct']=DF2["conversions"]/DF2["unique_devices_by_adid"]
#DF2=DF2[(DF2.convpct < 0.05) & (DF2.convpct != 0.0)]
#results=DF2[['avg_new_pv', 'convpct', 'adid']].groupby('adid').mean()



fig=plt.figure()
sns.jointplot(results.avg_new_pv,results['convpct'], kind='scatter')
os.chdir('conversion_plots')
plt.savefig('all_conversions.pdf')
os.chdir('..')

cc=DF2[['avg_new_pv', 'convpct', 'adid', 'advertiserid']].groupby(['advertiserid','adid'],\
                                                                  as_index=False).mean() 
dd=cc[['adid', 'advertiserid']].groupby(['advertiserid']).agg(['count'])
#advertisers=dd['adid']['count'].nlargest(20).index.values#[20006 , 21077,  30397,  20352,  19680,  21058,  30377,  31181,  32266,  30392 ] 
advertisers=[21077, 30377, 30397]
#smallest=[19225, 19235,19254, 19288, 19317, 19484, 19537, 19538, 19612, 19627 ]


os.chdir('casestudies')
colors=['r', 'b', 'g']
markers=['*', 'o', '^']
fig=plt.figure()
ax=fig.add_subplot(111)
ax.set_xlabel('Average Percent Viewed')
ax.set_ylabel('Conversions Per Device')
ax.set_yscale('log')
ax.set_title('3 Case Studies of Percent Viewed Vs Conversions')
for adv in range(len(advertisers)):
       
     DF3=DF[np.isnan(DF["conversions"])==False]
     DF3=DF3[DF3.advertiserid == advertisers[adv]]
     DF3=DF3.join(DF3.groupby('adid')['unique_devices'].sum(), on='adid', rsuffix='_by_adid')
     DF3['convpct']=DF3["conversions"]/DF3["unique_devices_by_adid"]
#          
     results=DF3[['avg_new_pv', 'convpct', 'adid']].groupby('adid').mean()
     results=results[results.convpct < 0.5]
     results=results[results.avg_new_pv >= 75]
     ax.scatter(results.avg_new_pv, results['convpct'], color=colors[adv], marker=markers[adv], alpha=0.75, edgecolors=None, s=30)
     fit = np.polyfit(np.sort(results.avg_new_pv), np.log10(results['convpct']), deg=1)
     polynomial = np.poly1d(fit)
     ys = polynomial(results.avg_new_pv)
     #ax.plot(results.avg_new_pv,10**ys, color=colors[adv], alpha=0.2, lw=2, ls='--')
#     fig=plt.figure()
#     sns.jointplot(results.avg_new_pv,np.log10(results['convpct']), kind='scatter')
#     plt.savefig('largest_'+str(adv)+'_casestudy.pdf')
plt.legend(advertisers, loc='lower right')
plt.savefig('conversions_case_studies.pdf')
#DF4=DF[np.isnan(DF["conversions"])==False]
#cc=DF2[['avg_new_pv', 'convpct', 'adid', 'advertiserid']].groupby(['advertiserid','adid'],\
#                                                                  as_index=False).mean() 
#dd=cc[['adid', 'advertiserid']].groupby(['advertiserid']).agg(['count'])
#DF4=DF4[DF4.advertiserid.isin(dd['adid']['count'].nsmallest(100).index.values)]
#DF4=DF4.join(DF4.groupby('adid')['unique_devices'].sum(), on='adid', rsuffix='_by_adid')
#DF4['convpct']=DF4["conversions"]/DF4["unique_devices_by_adid"]     
#results=DF4[['avg_new_pv', 'convpct', 'adid']].groupby('adid').mean()
#results=results[results.convpct < 0.5]
#fig=plt.figure()
#sns.jointplot(results.avg_new_pv,results['convpct'], kind='scatter')
#plt.savefig('smallest_'+str(adv)+'_casestudy.pdf')



     
os.chdir('..')






stop

#DF2=DF2[DF2.ad_id.isin((DF2['adid'].value_counts() < 10).index)]
#DF2=DF2[DF2['ad_id'].value_counts() >= 5]


import seaborn as sns
os.chdir('conversion_plots')

for iAd in DF2.ad_id.unique():
 try:
    testdf7=DF2[DF2.ad_id == iAd]
    #testdf7=testdf[testdf.attribution_window=='7d']
    testdf7['convpct']=testdf7.conversions/testdf7.unique_devices
    testdf7=testdf7[testdf7['convpct']- testdf7['convpct'].mean()< 2*testdf7['convpct'].std()]
    fig=plt.figure()
    sns.jointplot(testdf7.avg_new_pv,testdf7['convpct'], kind='scatter')
    plt.savefig(str(iAd)+'7day_conversions.pdf')
 except(KeyboardInterrupt):
     raise
 except:
     None
    
testdf7=testdf[testdf.attribution_window=='7d']
testdf7['convpct']=testdf7.conversions/testdf7.unique_devices
#testdf7=testdf7[testdf7['convpct']- testdf7['convpct'].mean()< 2*testdf7['convpct'].std()]
fig=plt.figure()
sns.jointplot(DF2.avg_new_pv,DF2['conversions'], kind='scatter')
plt.savefig('all_7day_conversions.pdf')

os.chdir('..')

# potential ads: 13529107, 14397077, 1415766,1251302
# best: 1294520, 1384716


# correlations real fast
#corr7day=scipy.stats.pearsonr(np.array(DF2[DF2.attribution_window == '7d']['avg_new_pv']), np.array(DF2[DF2.attribution_window == '7d']['conversions']/DF2[DF2.attribution_window == '7d']['unique_devices']))
#print(corr7day)




stop




#count=0
#dataList=[]
#for chunk in pd.read_csv('lois_nbc_grouped_by_occurrence_id.csv', delimiter=',', names=labels, chunksize=200000):
#    cc=chunk[chunk.seriesid.str.contains('EP')]
#    #cc=cc[cc.day_part_id ==6 ]
#    dataList.append(cc)
#
#DS=pd.concat(dataList, ignore_index=True)
#del dataList

# temp drop
DS=DS.drop(['occurrence_id'], axis=1)

DS=DS.dropna(axis=0)
DS=DS[DS.seriesid.str.contains('EP')]
DS=DS.drop(['occurrence_is_fta'], axis=1)
#DS["occurrence_is_fta"]=DS["occurrence_is_fta"].replace('f', 0)
#DS["occurrence_is_fta"]=DS["occurrence_is_fta"].replace('t', 1)

# now modify based on duration
DS['duration_sec'][DS.duration_sec == 9] = 10
DS['duration_sec'][DS.duration_sec == 12] = 10
DS['duration_sec'][DS.duration_sec == 14] = 15
DS['duration_sec'][DS.duration_sec == 19] = 20
DS['duration_sec'][DS.duration_sec == 29] = 30
DS['duration_sec'][DS.duration_sec == 59] = 60

# get a new column - days from airings
#"OccurenceDateTime", "first_airing_date"
DS['OccurenceDateTime']=pd.to_datetime(DS['OccurenceDateTime'])
DS['first_airing_date']=pd.to_datetime(DS['first_airing_date'])
DS['days_diff']=[int(i.days) for i in ( DS['OccurenceDateTime']- DS['first_airing_date'])]
DS.sort_values(['ad_id','OccurenceDateTime'],inplace=True)
#add cumulative count for each id.
DS['total_occ'] = DS.groupby('ad_id').cumcount()
DS['total_occ_channel']=DS.groupby(['ad_id','channel_id']).cumcount()
DS['total_occ_series']=DS.groupby(['ad_id','seriesid']).cumcount()


# duration by  day part
#pv15=DS[(DS.duration_sec>=10) & (DS.duration_sec<=20)]#['new_pv'].mean()
#pv30=DS[(DS.duration_sec>=25) & (DS.duration_sec<=35)]#['new_pv']#.mean()
#pv60=DS[(DS.duration_sec>=50) & (DS.duration_sec<=70)]#['new_pv'].mean()
#font = {'weight' : 'bold',
#        'size'   : 20}
#podletters=['A',  'M', 'Y' ]
#labels=['F', 'M', 'L']
#durations=['15 Second', '30 Second', '60 Second']
#durPV=[pv15, pv30, pv60]
#
#os.chdir('daypartPlots')
#for iday in range(1,len(DS.day_part_id.unique())+1):
#  matplotlib.rc('font', **font)
#  fig=plt.figure(figsize=(10,6))
#  ax=fig.add_subplot(111)
#  ax.grid(zorder=0)
#  ax.bar([0,1,2], [pv15[pv15.day_part_id==iday]['avg_new_pv'].mean()+5,pv30[pv30.day_part_id==iday]['avg_new_pv'].mean()+5,pv60[pv60.day_part_id==iday]['avg_new_pv'].mean()+5] , width=[0.5, 0.5, 0.5], align='center',color='green', alpha=0.5)
#  ax.set_ylim(80,100)
#  ax.set_title('Duration Variability For Ads for Day part = '+str(iday))
#  ax.set_xlabel('Duration')
#  ax.set_ylabel('Average Percent Viewed')
#  plt.xticks([0,1,2], ['15 Second', '30 Second', '60 Second' ])
#  plt.savefig('Duration Variability For Ads for Day part = '+str(iday)+'.pdf') 
#os.chdir('..')


# encode the features
# change the cardinality
labels=["day_part_id","ad_id","advertiser_id", "duration_sec", "pod_letter", "seriesid", "show_type", "channel_id", "avg_new_pv", "occurrence_is_fta", "days_airing"]
hi_card_features = ['ad_id', 'advertiser_id', 'seriesid','channel_id', 'show_type','day_part_id', 'duration_sec', 'pod_letter'  ]
low_card_features = []#['day_part_id', 'duration_sec', 'pod_letter', 'occurrence_is_fta']
regressor_features=['days_diff', 'total_occ', 'total_occ_channel']
agg_fun={'avg_new_pv':np.mean}


# split the data set

#train=DS.sample(frac=0.7,random_state=1)
#test=DS.drop(train.index)
train=DS[(DS.OccurenceDateTime <= np.datetime64('2016-10-11 00:00:00')) | (DS.OccurenceDateTime > np.datetime64('2016-10-18 00:00:00'))]
test=DS[(DS.OccurenceDateTime > np.datetime64('2016-10-11 00:00:00')) | (DS.OccurenceDateTime <= np.datetime64('2016-10-18 00:00:00'))]
train=train.drop(['OccurenceDateTime', 'first_airing_date'], axis=1)
test=test.drop(['OccurenceDateTime', 'first_airing_date'], axis=1)
#merged_1=deepcopy(train)
#merged_2=deepcopy(test)
merged_1=train
merged_2=test

for hi_card_feature in hi_card_features:
    col_list=low_card_features+[hi_card_feature]
    print col_list
    gb = train.groupby(col_list,as_index=False).agg(agg_fun)
    merged_1=merged_1.merge(gb,on=col_list,how='inner',suffixes=('','_'+hi_card_feature))
    merged_2=pd.merge(merged_2, merged_1[[hi_card_feature,'avg_new_pv_'+hi_card_feature]].drop_duplicates(subset=hi_card_feature), on=hi_card_feature, how='inner')

DFpreTRAIN=merged_1
DFpreTRAIN=DFpreTRAIN.drop(hi_card_features, axis=1)
DFpreTEST=merged_2
DFpreTEST=DFpreTEST.drop(hi_card_features, axis=1)
nLfeatures=['day_part_id', 'duration_sec','pod_letter',  'avg_new_pv_channel_id', 'occurrence_is_fta',
            'avg_new_pv_ad_id', 'avg_new_pv_advertiser_id','avg_new_pv_seriesid', 'avg_new_pv_show_type', 'days_diff', 'total_occ','total_occ_channel', 'total_occ_series']
#colsToChange=["day_part_id", "duration_sec", "pod_letter"]
#train= pd.get_dummies(DFpreTRAIN, columns = colsToChange)
#test= pd.get_dummies(DFpreTEST, columns = colsToChange)
train=DFpreTRAIN
test=DFpreTEST

model=RandomForestRegressor(max_features=0.25, n_jobs=4, verbose=3)
features=list(test.columns.values[1:])
model.fit(train[features], train['avg_new_pv'])
model.score(test[features], test['avg_new_pv'])
predictions=model.predict(test[features])
acc = r2_score(test.avg_new_pv, model.predict(test[features]))

importances = model.feature_importances_
std = np.std([tree.feature_importances_ for tree in model.estimators_],
             axis=0)
indices = np.argsort(importances)[::-1]

# Print the feature ranking
print("Feature ranking:")

listStr=[]
listVal=[]
for f in range(train[features].shape[1]):
    print("%d. feature %s (%f)" % (f + 1, features[indices[f]], importances[indices[f]]))
    listStr.append(features[indices[f]])
    listVal.append(importances[indices[f]]*100)

# sum the features
#listStr=list(listStr); listVal=np.array(listVal)
#colVals=[]
#names=nLfeatures
#for item in names:
#    matches=[listStr.index(s) for s in listStr if item in s]
#    colVals.append(np.sum(listVal[matches]))
#print(colVals)
#print(names)
newStr=[]
for item in listStr:
    if item[0:11]=='avg_new_pv_':
        newStr.append(item[11:])
    else:
        newStr.append(item)
      

sortedlist=[(x,int(y*1000.0)/1000.0) for (y,x) in sorted(zip(listVal,listStr))]
sortedlist2=[x for (y,x) in sorted(zip(listVal,listStr))]
print(sortedlist[::-1])
fig=plt.figure()
ax=fig.add_subplot(111)
plt.subplots_adjust(bottom=0.3, top=0.9, left=0.15)
ax.bar(range(train[features].shape[1]), importances[indices]*100.0,
       color="r", yerr=std[indices]*100., align="center")
ax.set_ylabel('Feature Importance (%)')
ax.set_xlabel('Features')
plt.xticks(range(train[features].shape[1]),newStr , rotation=60)
plt.savefig('RFfeature_importances_test4.pdf')

# combined features
Creative=listVal[0]+listVal[1]+listVal[11]
Show=listVal[3]+listVal[8]+listVal[9]+listVal[10]+listVal[6]
Decay=listVal[2]+listVal[4]+listVal[5]+listVal[7]

names=['Creative', 'Show', 'Decay']
weights=[Creative, Show, Decay]

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 22}

matplotlib.rc('font', **font)
fig=plt.figure()
ax=fig.add_subplot(111)
plt.subplots_adjust(bottom=0.3, top=0.9, left=0.15)
ax.bar(range(3), weights ,
       color="g",align="center", alpha=0.5)
ax.grid(True)
ax.set_ylabel('Feature Importance (%)')
ax.set_xlabel('Features')
plt.xticks(range(3), names , rotation=0)
plt.savefig('RFfeature_importances_summed.pdf')

# the weights are:
#[('ad_id', 42.7), ('advertiser_id', 26.7), ('seriesid', 9.8), ('pod_letter', 5.5), ('show_type', 4.6), ('day_part_id', 3.7), ('duration_sec', 3.5), ('channel_id', 2.2), ('occurrence_is_fta', 0.8)]


print(acc)

print('percentage within 3%')
test1=np.abs(predictions-np.array(test.avg_new_pv))
print(100.0*len(test1[test1<=3])/(1.0*len(test1)))


#from Elliott tweaks
#
#[('avg_new_pv_ad_id', 56.6), ('avg_new_pv_advertiser_id', 32.8), ('avg_new_pv_seriesid', 5.4), ('avg_new_pv_show_genre', 2.0), ('day_part_id', 0.9), ('duration_sec', 0.9), ('pod_letter', 0.5), ('channel_id', 0.4), ('occurrence_is_fta', 0.0)]
#0.787639011479
#percentage within 3%
#79.7903789304

# try dropping out features and see what happens


#
# mutate daypart first
#random data
# statistics 
#d_overall_variability=[]
#import random
#randList=random.sample(range(len(test[features])), 100)
#for ir in randList:
#    predictor=test[features].iloc[[ir]]
#    for item in ['15', '30', '60']:
#        predictor['duration_sec_'+str(int(item))]=0
#    values=[]
#    for item in ['15', '30', '60']:
#        temp=predictor
#        temp['duration_sec_'+str(int(item))]=1
#        # now test
#        values.append(model.predict(temp))
#    d_overall_variability.append(np.max(values)-np.min(values))
#
#print('duration mean: '+str(np.mean( d_overall_variability)))
#
#    
#daypart_overall_variability=[]
#randList=random.sample(range(len(test[features])), 100)
#for ir in randList:
#    predictor=test[features].iloc[[ir]]
#    for item in range(1,10):
#        predictor['day_part_id_'+str(float(item))]=0
#    values=[]
#    for item in range(1,10):
#        temp=predictor
#        temp['day_part_id_'+str(float(item))]=1
#        # now test
#        values.append(model.predict(temp))
#    daypart_overall_variability.append(np.max(values)-np.min(values))
#print('daypart mean: '+str(np.mean(daypart_overall_variability)))



# case studies
#rdata=[2600,200,2000,20000]
#overall_values=[]
#for ir in rdata:
#    predictor=test[features].iloc[[ir]]
#    for item in ['15', '30', '60']:
#        predictor['duration_sec_'+str(int(item))]=0
#    values=[]
#    for item in ['15', '30', '60']:
#        temp=predictor
#        temp['duration_sec_'+str(int(item))]=1
#        # now test
#        values.append(model.predict(temp))
#    overall_values.append(values)  
##
## now plot it
#os.chdir('daypartPlots')
#fig=plt.figure(figsize=(10,8))
#ax=fig.add_subplot(111)
#colors=['r', 'b', 'g', 'DarkOrange']
#for item in range(len(colors)):
#    ax.plot(range(3), overall_values[item], color=colors[item], marker='o')
#ax.set_ylabel('Predicted Percent Viewed')
#ax.set_xlabel('Duration')
#ax.set_xlim([-0.5, 2.5])
#ax.set_xticks([0,1,2])
#ax.set_xticklabels( ['15', '30', '60'])
#plt.savefig('duration2_overall.pdf')
#os.chdir('..')


#  2700 in the test, 1504203
# 100, 1506692
# 1000,1516790
# 10000, 1381629
#'1381629','Garnier Nutrisse Ultra Color TV Spot, \'Are You Ready?\''
#'1504203','Gain Flings! Super Bowl 2017 TV Spot, \'Getting Sentimental\' Ft. Ty Burrell'
#'1506692','2017 Buick Envision TV Spot, \'2017 March Madness: People Are Talking\''
#'1516790','L\'Oreal Paris Feria Multi-Faceted Shimmering Color Rose Gold TV Spot, \'Dye\''
