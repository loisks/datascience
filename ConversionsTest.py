# RFtest1.py
#
# Random Forest Model for Feature Ranking for NBC
# using percent viewed
# LKS, May 2017
#

#
# put ad decay for a quarter
# variability of model
# [CHECK]loss per second on duration plot instead of average percent viewed 
#
# tomorrow first thing
# literally this hasn't changed 
# (1) Seans' ad wear weight watchers thing
# (2) continue working on model, ad in new data set 
# 
#
#

import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import seaborn as sns
import scipy.stats
import pandas as pd
import matplotlib
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import roc_auc_score, r2_score
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.cross_validation import train_test_split
from copy import deepcopy

# labels

#labels=['day_part_id', 'adid', 'advertiserid', 'duration_sec', 'pod_letter', 'seriesid', 'show_type', 'channel_id', 'avg_new_pv', 'occurrence_is_fta', 'occurrence_id', 'occurrence_airing_date', 'first_airing_date', 'unique_devices', 'unique_dates', 'unique_attribution_windows', 'conversions']
labels=['day_part_id','adid','advertiserid','duration_sec','pod_letter','seriesid','show_type','channel_id','avg_new_pv','occurrence_is_fta','occurrence_id','occurrence_airing_date','first_airing_date','unique_devices','conversions','impression_time', 'conversion_time', 'site_id', 'conversion_type']

DS=pd.read_csv('lois_for_conversions_output_v1.csv', names=labels)

#DS=DS[DS.site_id != 'TC-1002-2']
#DS=DS[DS.site_id != 'TC-1002-1']

DF=DS[["occurrence_id", "adid", "advertiserid", "avg_new_pv", "conversions", "unique_devices", 'duration_sec', 'conversion_type']]

#DF=DF[DF.conversions > 2]
DFAll=DF
DF2=DF[DF['conversion_type']!='visit']
DF=DF[DF['conversion_type']=='visit']
DF['convRate']=DF.conversions/DF.unique_devices
#DF=DF[DF['avg_new_pv']> 65]
DF2['convRate']=DF2.conversions/DF2.unique_devices
DF2=DF2[DF2['avg_new_pv']> 65]

DFAll=DFAll[DFAll.avg_new_pv > 65]
DFAll['convRate']=DFAll.conversions/DFAll.unique_devices

DFspec=DF[DF.advertiserid==35263]







for adi in DF.advertiserid.unique():
    t=DF[DF.advertiserid==adi]
    fig=plt.figure()
    ax=fig.add_subplot(111)
    sns.jointplot(t.avg_new_pv,t['convRate'], kind='scatter', color='green', alpha=0.5)
    os.chdir('conversion_plots')
    plt.savefig('all_conversions_visits_'+str(adi)+'_all.pdf')
    os.chdir('..')
    plt.clf()

fig=plt.figure()
ax=fig.add_subplot(111)
sns.jointplot(DF2.avg_new_pv,DF2['convRate'], kind='scatter', color='green', alpha=0.5)
os.chdir('conversion_plots')
plt.savefig('all_conversions_non_visits_all.pdf')
os.chdir('..')
plt.clf()




# look at different adids within campaign
for adid in DFspec.adid.unique():
    try:
        t2=DFspec[DFspec.adid==adid]
        fig=plt.figure()
        ax=fig.add_subplot(111)
        sns.jointplot(t2.avg_new_pv,t2['convRate'], kind='scatter', color='green', alpha=0.5)
        os.chdir('conversion_plots')
        plt.savefig('all_conversions_visits_advetiser=31535_adid='+str(adid)+'all_.pdf')
        os.chdir('..')
        plt.clf()
    except:
        None


# look at a few campaigns




adv=[35263, 31206]


os.chdir('conversion_plots') 
colors=[ 'b', 'g']
markers=[ 'o', '^']
fig=plt.figure()
ax=fig.add_subplot(111)
ax.set_xlabel('Average Percent Viewed')
ax.set_ylabel('Conversion Rate (%) ')
ax.set_title('Percent Viewed Vs Conversions')
ax.scatter(DF[DF.advertiserid==adv[0]].avg_new_pv, DF[DF.advertiserid==adv[0]]['convRate']*100., color=colors[0], marker=markers[0], alpha=0.1, edgecolors=None, s=30)
ax.set_xlim(60,100)
#ax.scatter(DF[DF.advertiserid==adv[1]].avg_new_pv, DF[DF.advertiserid==adv[1]]['convRate']*100., color=colors[1], marker=markers[1], alpha=0.1, edgecolors=None, s=30)
ax.set_ylim(0, 0.6)
#fit = np.polyfit(np.sort(DF[DF.advertiserid==adv[0]].avg_new_pv), DF[DF.advertiserid==adv[0]]['convRate'], deg=1)
#polynomial = np.poly1d(fit)
#ys = polynomial(DF[DF.advertiserid==adv[0]].avg_new_pv)
#ax.plot(DF[DF.advertiserid==adv[0]].avg_new_pv,ys, color=colors[0], alpha=0.2, lw=2, ls='--')
#
#fit = np.polyfit(np.sort(DF[DF.advertiserid==adv[1]].avg_new_pv), DF[DF.advertiserid==adv[1]]['convRate'], deg=1)
#polynomial = np.poly1d(fit)
#ys = polynomial(DF[DF.advertiserid==adv[1]].avg_new_pv)
#
#ax.plot(DF[DF.advertiserid==adv[1]].avg_new_pv,ys, color=colors[1], alpha=0.2, lw=2, ls='--')
plt.legend(['Case Study'], loc='upper left')
plt.savefig('conversions_case_studies_anonymous_final2.pdf')

print('made it here') 
os.chdir('..')


pch=[20827]



os.chdir('conversion_plots') 
colors=[ 'b', 'g']
markers=[ 'o', '^']
fig=plt.figure()
ax=fig.add_subplot(111)
ax.set_xlabel('Average Percent Viewed')
ax.set_ylabel('Conversion Rate (%) ')
ax.set_title('Publishers Clearing House Percent Viewed Vs Conversions')
ax.scatter(DF[DF.advertiserid==pch[0]].avg_new_pv, DF[DF.advertiserid==pch[0]]['convRate']*100., color=colors[0], marker=markers[0], alpha=0.1, edgecolors=None, s=30)
#ax.scatter(DF[DF.advertiserid==adv[1]].avg_new_pv, DF[DF.advertiserid==adv[1]]['convRate']*100., color=colors[1], marker=markers[1], alpha=0.1, edgecolors=None, s=30)
ax.set_ylim(0, 2.0)
ax.set_xlim(10,101)
plt.legend(['Publishers Clearing House'], loc='upper left')
plt.savefig('pchAll.pdf')

print('made it here') 
os.chdir('..')


stop
#DF2=DF[np.isnan(DF["conversions"])==False]

