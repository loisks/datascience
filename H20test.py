# H2OTest.py
#
# Random Forest Model for Feature Ranking for NBC
# using percent viewed
# goal here is to test the impact of changing each feature slightly
# LKS, May 2017
# try to use H2O
#

# 
# think about the best way to test each feature 
#

import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import h2o
import pandas as pd
import matplotlib
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import roc_auc_score, r2_score, accuracy_score
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.cross_validation import train_test_split
from h2o.estimators.gbm import H2OGradientBoostingEstimator
from h2o.estimators.random_forest import H2ORandomForestEstimator

# labels

from datetime import date, datetime

Y = 2000 # dummy leap year to allow input X-02-29 (leap day)
seasons = [('winter', (date(Y,  1,  1),  date(Y,  3, 20))),
           ('spring', (date(Y,  3, 21),  date(Y,  6, 20))),
           ('summer', (date(Y,  6, 21),  date(Y,  9, 22))),
           ('autumn', (date(Y,  9, 23),  date(Y, 12, 20))),
           ('winter', (date(Y, 12, 21),  date(Y, 12, 31)))]

def get_season(now):
    if isinstance(now, datetime):
        now = now.date()
    now = now.replace(year=Y)
    return next(season for season, (start, end) in seasons
                if start <= now <= end)

def is_broadcast(channel):
    if channel in (105,107, 104, 113) :
        out=1
    else:
        out=2
    return(out)


labels1=["day_part_id","adid","advertiserid", "duration_sec", "pod_letter", "seriesid", "show_type", "channel_id", "avg_new_pv", "occurrence_is_fta", "occurrence_id", "occurrence_airing_date", "first_airing_date"]

labels=["day_part_id", "adid", "advertiserid", "duration_sec", "pod_letter", "seriesid", \
        "show_type", "channel_id", "avg_new_pv", "occurrence_is_fta", "occurrence_id", \
        "occurrence_airing_date", "first_airing_date", "unique_devices", "unique_dates",\
        "unique_attribution_windows", "conversions"]

DS1=pd.read_csv('lois_nbc_q4_v1_grouped.csv', names=labels1)
DS2=pd.read_csv('lois_nbc_output_v10.csv', names=labels)

DS2=DS2.drop(['occurrence_id',"unique_devices", "unique_dates",\
        "unique_attribution_windows", "conversions"], axis=1)
DS1=DS1.drop(['occurrence_id'], axis=1)

DS=pd.concat([DS1, DS2], ignore_index=True)

# temp drop


DS=DS.dropna(axis=0)
DS=DS[DS.seriesid.str.contains('EP')]
DS=DS.drop(['occurrence_is_fta'], axis=1)
#DS["occurrence_is_fta"]=DS["occurrence_is_fta"].replace('f', 0)
#DS["occurrence_is_fta"]=DS["occurrence_is_fta"].replace('t', 1)

# now modify based on duration
DS['duration_sec'][DS.duration_sec <= 12] = 10
DS['duration_sec'][(DS.duration_sec > 12) & (DS.duration_sec < 19) ] = 15
DS['duration_sec'][(DS.duration_sec >= 19) & (DS.duration_sec < 23) ] = 20
DS['duration_sec'][(DS.duration_sec >= 23) & (DS.duration_sec < 27) ] = 25
DS['duration_sec'][(DS.duration_sec >= 27) & (DS.duration_sec < 35) ] = 30
DS['duration_sec'][(DS.duration_sec >= 40) & (DS.duration_sec < 50) ] = 45
DS['duration_sec'][(DS.duration_sec >= 50) & (DS.duration_sec < 71) ] = 60
DS['duration_sec'][(DS.duration_sec >= 70) & (DS.duration_sec < 80) ] = 75
DS['duration_sec'][DS.duration_sec >80 ] = 100

# make a new column
DS['length']=DS.duration_sec
DS['length'][(DS.duration_sec < 25)]='short'
DS['length'][(DS.duration_sec >= 25) & (DS.duration_sec < 40)]='medium'
DS['length'][(DS.duration_sec >= 40) & (DS.duration_sec < 101)]='long'




DS['occurrence_airing_date']=pd.to_datetime(DS['occurrence_airing_date'])
DS['first_airing_date']=pd.to_datetime(DS['first_airing_date'])
DS['days_diff']=[int(i.days) for i in ( DS['occurrence_airing_date']- DS['first_airing_date'])]
DS.sort_values(['adid','occurrence_airing_date'],inplace=True)
#add cumulative count for each id.
DS['total_occ'] = DS.groupby('adid').cumcount()
DS['total_occ_channel']=DS.groupby(['adid','channel_id']).cumcount()
DS['total_occ_series']=DS.groupby(['adid','seriesid']).cumcount()

DS['season']= DS['occurrence_airing_date'].apply(get_season)

h2o.init()
test=DS[(DS['occurrence_airing_date'] > '2016-11-01') & (DS['occurrence_airing_date'] < '2016-11-10')]
train=DS[(DS['occurrence_airing_date'] <= '2016-11-01') | (DS['occurrence_airing_date'] >= '2016-11-10')]
test.to_csv('test.csv')
train.to_csv('train.csv')





test = h2o.import_file("test.csv")
train = h2o.import_file("train.csv")

#test=covtype_df[(covtype_df['occurrence_airing_date'] > '2016-11-01') & (covtype_df['occurrence_airing_date'] < '2016-11-10')]

test=test.drop(['C1', 'occurrence_airing_date'], axis=1)
train=train.drop(['C1', 'occurrence_airing_date'], axis=1)
#train, valid, test = covtype_df.split_frame([0.8, 0.1], seed=1234)
cc=train.col_names
cc.remove('avg_new_pv')
covtype_X = cc
covtype_y = 'avg_new_pv'
rf_v1 = H2ORandomForestEstimator(
    model_id="rf_covType_v1",
    ntrees=200,
    stopping_rounds=2,
    score_each_iteration=True,
    seed=1000000)
rf_v1.train(covtype_X, covtype_y, training_frame=train)
rf_v1
rf_v1.score_history()
rf_v1.r2()

stop


# encode the features
# change the cardinality
labels=["day_part_id","adid","advertiserid", "duration_sec", "pod_letter", "seriesid", "show_type", "channel_id", "avg_new_pv", "occurrence_is_fta", "days_airing"]
hi_card_features = ['adid', 'advertiserid', 'seriesid','channel_id', 'show_type','day_part_id', 'duration_sec', 'pod_letter', 'season']
low_card_features = []#['day_part_id', 'duration_sec', 'pod_letter', 'occurrence_is_fta']
regressor_features=['days_diff', 'total_occ', 'total_occ_channel']
agg_fun={'avg_new_pv':np.mean}


# split the data set

#train=DS.sample(frac=0.8,random_state=1)
#test=DS.drop(train.index)
train=DS[(DS.occurrence_airing_date <= np.datetime64('2016-10-11 00:00:00')) | (DS.occurrence_airing_date > np.datetime64('2016-10-18 00:00:00'))]
test=DS[(DS.occurrence_airing_date > np.datetime64('2016-10-11 00:00:00')) | (DS.occurrence_airing_date <= np.datetime64('2016-10-18 00:00:00'))]


train=train.drop(['occurrence_airing_date', 'first_airing_date'], axis=1)
test=test.drop(['occurrence_airing_date', 'first_airing_date'], axis=1)
merged_1=train
merged_2=test

for hi_card_feature in hi_card_features:
    col_list=low_card_features+[hi_card_feature]
    print col_list
    gb = train.groupby(col_list,as_index=False).agg(agg_fun)
    merged_1=merged_1.merge(gb,on=col_list,how='inner',suffixes=('','_'+hi_card_feature))
    merged_2=pd.merge(merged_2, merged_1[[hi_card_feature,'avg_new_pv_'+hi_card_feature]].drop_duplicates(subset=hi_card_feature), on=hi_card_feature, how='inner')

DFpreTRAIN=merged_1
DFpreTRAIN=DFpreTRAIN.drop(hi_card_features, axis=1)
DFpreTEST=merged_2
DFpreTEST=DFpreTEST.drop(hi_card_features, axis=1)

train=DFpreTRAIN
test=DFpreTEST
# 30 was really good for n_estimators
model=RandomForestRegressor(n_estimators=10,max_features=0.65, n_jobs=4, verbose=3)#, min_samples_leaf=5)
features=list(test.columns.values[1:])
model.fit(train[features], train['avg_new_pv'])
predictions=model.predict(test[features])

acc = r2_score(test.avg_new_pv, model.predict(test[features]))

importances = model.feature_importances_
std = np.std([tree.feature_importances_ for tree in model.estimators_],
             axis=0)
indices = np.argsort(importances)[::-1]

# Print the feature ranking
print("Feature ranking:")

listStr=[]
listVal=[]
for f in range(train[features].shape[1]):
    print("%d. feature %s (%f)" % (f + 1, features[indices[f]], importances[indices[f]]))
    listStr.append(features[indices[f]])
    listVal.append(importances[indices[f]]*100)

# sum the features
#listStr=list(listStr); listVal=np.array(listVal)
#colVals=[]
#names=nLfeatures
#for item in names:
#    matches=[listStr.index(s) for s in listStr if item in s]
#    colVals.append(np.sum(listVal[matches]))
#print(colVals)
#print(names)
newStr=[]
for item in listStr:
    if item[0:11]=='avg_new_pv_':
        newStr.append(item[11:])
    else:
        newStr.append(item)
      

sortedlist=[(x,int(y*1000.0)/1000.0) for (y,x) in sorted(zip(listVal,listStr))]
sortedlist2=[x for (y,x) in sorted(zip(listVal,listStr))]
print(sortedlist[::-1])
fig=plt.figure()
ax=fig.add_subplot(111)
plt.subplots_adjust(bottom=0.3, top=0.9, left=0.15)
ax.bar(range(train[features].shape[1]), importances[indices]*100.0,
       color="r", yerr=std[indices]*100., align="center")
ax.set_ylabel('Feature Importance (%)')
ax.set_xlabel('Features')
plt.xticks(range(train[features].shape[1]),newStr , rotation=60)
plt.savefig('RFfeature_importances_test5.pdf')

# combined features
#Creative=[importances


# WEIGHTED PLOT
creatives=34.582+13.248+1.405
decay= 9.897+7.864+3.386
show = 7.224+ 6.077+5.561+3.286+3.121+ 2.446+ 1.896

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 22}

matplotlib.rc('font', **font)

fig=plt.figure()
ax=fig.add_subplot(111)
plt.subplots_adjust(bottom=0.3, top=0.9, left=0.15)
ax.bar([0,1,2], [creatives, decay, show],
       color="g",  align="center", alpha=0.5)
ax.set_ylabel('Feature Importance (%)')
ax.set_xlabel('Features')
ax.grid(True)
plt.xticks([0,1,2], ['Creative', 'Decay', 'Show'])
plt.savefig('RFfeature_summed_updated1.pdf')

# the weights are:
#[('ad_id', 42.7), ('advertiser_id', 26.7), ('seriesid', 9.8), ('pod_letter', 5.5), ('show_type', 4.6), ('day_part_id', 3.7), ('duration_sec', 3.5), ('channel_id', 2.2), ('occurrence_is_fta', 0.8)]


print(acc)

print('percentage within 5%')
test1=np.abs(predictions-np.array(test.avg_new_pv))
print(100.0*len(test1[test1<=5])/(1.0*len(test1)))


#from Elliott tweaks
#
#[('avg_new_pv_ad_id', 56.6), ('avg_new_pv_advertiser_id', 32.8), ('avg_new_pv_seriesid', 5.4), ('avg_new_pv_show_genre', 2.0), ('day_part_id', 0.9), ('duration_sec', 0.9), ('pod_letter', 0.5), ('channel_id', 0.4), ('occurrence_is_fta', 0.0)]
#0.787639011479
#percentage within 3%
#79.7903789304

# try dropping out features and see what happens


#
# mutate daypart first
#random data
# statistics 
#d_overall_variability=[]
#import random
#randList=random.sample(range(len(test[features])), 100)
#for ir in randList:
#    predictor=test[features].iloc[[ir]]
#    for item in ['15', '30', '60']:
#        predictor['duration_sec_'+str(int(item))]=0
#    values=[]
#    for item in ['15', '30', '60']:
#        temp=predictor
#        temp['duration_sec_'+str(int(item))]=1
#        # now test
#        values.append(model.predict(temp))
#    d_overall_variability.append(np.max(values)-np.min(values))
#
#print('duration mean: '+str(np.mean( d_overall_variability)))
#
#    
#daypart_overall_variability=[]
#randList=random.sample(range(len(test[features])), 100)
#for ir in randList:
#    predictor=test[features].iloc[[ir]]
#    for item in range(1,10):
#        predictor['day_part_id_'+str(float(item))]=0
#    values=[]
#    for item in range(1,10):
#        temp=predictor
#        temp['day_part_id_'+str(float(item))]=1
#        # now test
#        values.append(model.predict(temp))
#    daypart_overall_variability.append(np.max(values)-np.min(values))
#print('daypart mean: '+str(np.mean(daypart_overall_variability)))



# case studies
#rdata=[2600,200,2000,20000]
#overall_values=[]
#for ir in rdata:
#    predictor=test[features].iloc[[ir]]
#    for item in ['15', '30', '60']:
#        predictor['duration_sec_'+str(int(item))]=0
#    values=[]
#    for item in ['15', '30', '60']:
#        temp=predictor
#        temp['duration_sec_'+str(int(item))]=1
#        # now test
#        values.append(model.predict(temp))
#    overall_values.append(values)  
##
## now plot it
#os.chdir('daypartPlots')
#fig=plt.figure(figsize=(10,8))
#ax=fig.add_subplot(111)
#colors=['r', 'b', 'g', 'DarkOrange']
#for item in range(len(colors)):
#    ax.plot(range(3), overall_values[item], color=colors[item], marker='o')
#ax.set_ylabel('Predicted Percent Viewed')
#ax.set_xlabel('Duration')
#ax.set_xlim([-0.5, 2.5])
#ax.set_xticks([0,1,2])
#ax.set_xticklabels( ['15', '30', '60'])
#plt.savefig('duration2_overall.pdf')
#os.chdir('..')


#  2700 in the test, 1504203
# 100, 1506692
# 1000,1516790
# 10000, 1381629
#'1381629','Garnier Nutrisse Ultra Color TV Spot, \'Are You Ready?\''
#'1504203','Gain Flings! Super Bowl 2017 TV Spot, \'Getting Sentimental\' Ft. Ty Burrell'
#'1506692','2017 Buick Envision TV Spot, \'2017 March Madness: People Are Talking\''
#'1516790','L\'Oreal Paris Feria Multi-Faceted Shimmering Color Rose Gold TV Spot, \'Dye\''
