# NBCAdids.py
#
# NBC is going to give us a list of ad ids
# from this we need to get
#
# * a showType retention score for each adid
# * a non-showType retentention score
# * an estimated retention score if the two above are not applicable
#   (i.e. first time airing)
#
# ** Returns **
# rank order, also put a standard deviation on this so we can guarantee
#  we have the best ads
# how many times this ad has aired before
#
# LKS, January 2017
#
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
from sqlalchemy import create_engine
import json
import seaborn as sns
#
#
#
# list of adids
#adids=[1002063,1084108,1084251,1176924,1183384,1194444,1240534,1240871,1243737
#,1263928]
title='superstore'
adids=[1435688, \
       1468847, \
       1457033, \
       1468022, \
1467407]
showGoal='Sitcom'
podletterGoal='None'
dirName='Test2'
datebegin='20170115'
dateend='20170131'

#
#
# create the engines
def create_db_engine(db_type, host, port, database, user, password):
    return create_engine(db_type + '://' + user + ':' + password + '@' + host + ':' + port + '/' + database)


def create_db_engine_with_credentials(db_cred_file_name, conn_name):
    with open(db_cred_file_name) as json_file:

        db_credentials = json.load(json_file)
        conn = db_credentials[conn_name]
        # print type(conn['port'])
        return create_db_engine(conn['db_type'], conn['host'], conn['port'], conn['database'],
                                conn['user'], conn['password'])

redshift_engine = create_db_engine_with_credentials('/audience-core/credentials/db_credentials.json', 'ispot_redshift')
mysql_engine = create_db_engine_with_credentials('/audience-core/credentials/db_credentials.json', 'ispot_db_mysql')


def queryAd(adID):
    # input
    # adID = an iSpot adID
    #
    # returns
    # a pandas dataframe to use to calculate a tune out rate 
    #
    # give an ad id to calculate it's retention score
    query=\
    """ SELECT sc.name showType,
               series.title series_title,
               occ.Segment as PodNum,
               occ.SegmentOrder as Spot,
               
               avd.start_total_impressions, 	  
               avd.total_impressions,
               avd.start, 
               avd.first_qt, 
               avd.second_qt, 
               avd.third_qt, 
               avd.full,  
               ama.ad_title,            
               sched.program_airing_id,               
               occ.OccurenceID, 
               occ.pod_letter
        FROM ispot_data.master_dsd mdsd
        JOIN ispot_data.ad_meta_national ama on mdsd.adid = ama.adid
        JOIN ispot_data.airings_detail ad on mdsd.dsd_id = ad.dsd_id
        JOIN ispot_data.audience_detail_occurrence ado on ad.occurrenceid = ado.occurrenceid
        JOIN ispot_data.audience_view_detail avd on avd.occurrenceid = ado.occurrenceid
        JOIN ispot_db.series series on mdsd.seriesid=series.seriesid
        JOIN ispot_db.seriescategory programs on mdsd.seriesid=programs.seriesid
        JOIN ispot_db.schedules sched on mdsd.scheduleid=sched.scheduleid
        JOIN ispot_db.showcategories sc on programs.showcategoryid = sc.showcategoryid
        RIGHT JOIN ispot_db.adoccurence occ on ado.occurrenceid = occ.OccurenceID
        WHERE  mdsd.date_est BETWEEN {0} AND {1}
        AND avd.start_total_impressions > 20
        AND ama.adid = {2}; """.format(datebegin, dateend, adID)
    Q1 = pd.read_sql_query(query, con=mysql_engine, coerce_float=False)
    return Q1


def get_tune_out(row):
  #
  # input
  # row = row of pandas dataframe
  #
  # output
  # tuneout rate row and other things
  #
  if row.start == 0:
    # redistribute the wealth
    tstart = (.7*row.first_qt)+(.2*row.second_qt)+(.01*row.third_qt)+\
             (.01*row.full)
    tfirst_qt=row.first_qt- .7*row.first_qt
    tsecond_qt=row.second_qt-.2*row.second_qt
    tthird_qt=row.third_qt- .01*row.third_qt
    tfull =row.full- .01*row.full
    
    # calculate tune out rate from this 
    if (tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (1-((tfull+tthird_qt)/row.start_total_impressions))
        method=5
    elif (tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (1-((tfull+tthird_qt+tsecond_qt)/row.start_total_impressions))
        method=6
    elif (tfirst_qt+tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (1-((tfull+tthird_qt+tsecond_qt+tfirst_qt)/row.start_total_impressions))
        method=7
    else:
        r = np.array([ tfirst_qt, tsecond_qt, tthird_qt, tfull])
        c=np.max((r/row.start_total_impressions))
        tune_out = 100*(c)                  
        method=8 # the worst method    
    
  else:       
    # the best data 
    if (row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (1-((row.full+row.third_qt)/row.start_total_impressions))
        method=1
    elif (row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (1-((row.full+row.third_qt+row.second_qt)/row.start_total_impressions))
        method=2
    elif (row.first_qt+row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (1-((row.full+row.third_qt+row.second_qt+row.first_qt)/row.start_total_impressions))
        method=3   
    else:    
        r = np.array([ row.first_qt, row.second_qt, row.third_qt, row.full])
        c=np.max((r/row.start_total_impressions))
        tune_out = 100*(c)                  
        method=4
  if np.isnan(tune_out) == True:
    tune_out=-1
  tune_out=100-int(100*tune_out)/100.
  return pd.Series({'method': method, 'tune_out': tune_out, 'podNum':row.PodNum, \
                    'spot':row.Spot, 'showType':row.showType, 'ad_title':row.ad_title, \
                    'series_title':row.series_title, 'pod_letter':row.pod_letter })
#
#
def num2float(df):
    for col in df.columns:
        try:
            df[col] = df[col].astype(float)
        except:
            print col+' column cannot be float-ized'
    return df


#
#
# START THE SCRIPT 
#
#
#
# make the directory to save everything
#
if not os.path.exists(dirName):
    os.makedirs(dirName)

#
#
# loop through ads
file= open(title+'AdExamples.txt', 'wb')
for iAd in range(len(adids)):
    print('Ad id is: '+ str(adids[iAd]))
    # first get the historical data for this 
    df= queryAd(adids[iAd])

    # then calculate tune out rates 
    df=num2float(df)
    dfproc=df.apply(get_tune_out, axis=1)
    
    # cascade now
    #
    # first try by show type
    try:
     if len(dfproc[dfproc.showType==showGoal]) > 10:
        temp1=dfproc[dfproc.showType==showGoal]
        showTypeCheck='yes'
        # check for pod letter 
        if len(temp1[temp1.pod_letter == podletterGoal]) > 10:
            ndf=dfproc[(dfproc.showType==showGoal) & (dfproc.pod_letter == podletterGoal)]
            plcheck='yes'
        else:
            ndf=dfproc[(dfproc.showType==showGoal) ]
            plcheck='no'

     else:
        showTypeCheck='no'
        if len(dfproc[dfproc.pod_letter == podletterGoal]) > 10:
            plcheck='yes'
            ndf=dfproc[dfproc.pod_letter == podletterGoal]


        else:
            plcheck='no'
            ndf=dfproc
 

     print('Show Type Check: ' + str(showTypeCheck) + ', Pod Letter Check: '+ str(plcheck))
     print('Using Show Type, with ' + str(len(ndf)) + ' occurrences')
     print('Viewability score is: '+ str(ndf['tune_out'].mean()))
     print('Standard deviation is: '+str(ndf['tune_out'].std()))

     if plcheck == 'yes':
        print('alternative outcome')
        if showTypeCheck=='yes':
            ndf=dfproc[dfproc.showType == showGoal]
        else:
            ndf=dfproc
        print('Viewability score is: '+ str(ndf['tune_out'].mean()))
        print('Standard deviation is: '+str(ndf['tune_out'].std()))
    
    
    #
    # get the pod letter distribution
     my_dict = {i:list(ndf['pod_letter']).count(i) for i in np.array(ndf['pod_letter'])}
     print(my_dict)

     file.write(str(adids[iAd]))
     file.write('\n')
     file.write(str(ndf['tune_out'].mean()))
     file.write('\n')
     file.write(str(ndf['tune_out'].std()))
     file.write('\n')
     file.write(str(my_dict))
     file.write('\n')
     
     # tomorrow test to see if cascade with pod letter or day part is worth it 
     #podLetter_tuneOuts=ndf.groupby('pod_letter')['tune_out']
     #mPL=podLetter_tuneOuts.mean()
     #stdPL=podLetter_tuneOuts.std()
     # pod letter definitely worth it
     
     os.chdir(dirName)    
     sns.set_style("whitegrid")   
     ax = sns.barplot(x="pod_letter", y="tune_out", data=ndf)
     ax.set(ylim=(60,100))
     plt.savefig('adid='+str(adids[iAd])+'_viewabilityScore.png')
     plt.close()
     os.chdir('..')
    except:
       None
file.close()
