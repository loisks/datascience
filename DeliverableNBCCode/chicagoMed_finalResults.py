# finaleResults.py
#
# the outcome of our NBC test!
#
# LKS, February 2017
#
#
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
from scipy.stats import ttest_ind
from scipy.stats import pearsonr

title='chicagoMed'
data=pd.read_csv(title+'_results.csv', sep=';', header=0)

# OTHER PODS
def interpolate_nans(X):
         """Overwrite NaNs with column value interpolations."""
         mask_j = np.isnan(X)
         X[mask_j] = np.interp(np.flatnonzero(mask_j), np.flatnonzero(~mask_j), X[~mask_j])
         return X

def get_tune_out(row):
  #
  # input
  # row = row of pandas dataframe
  #
  # output
  # tuneout rate row and other things
  #
  if row.start == 0:
    # redistribute the wealth
    tstart = (.7*row.first_qt)+(.2*row.second_qt)+(.01*row.third_qt)+\
             (.01*row.full)
    tfirst_qt=row.first_qt- .7*row.first_qt
    tsecond_qt=row.second_qt-.2*row.second_qt
    tthird_qt=row.third_qt- .01*row.third_qt
    tfull =row.full- .01*row.full
    
    # calculate tune out rate from this 
    if (tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt)/(1.0*row.start_total_impressions)))
        method=5
    elif (tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt+tsecond_qt)/(1.0*row.start_total_impressions)))
        method=6
    elif (tfirst_qt+tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt+tsecond_qt+tfirst_qt)/(1.0*row.start_total_impressions)))
        method=7
    else:
        r = np.array([ tfirst_qt, tsecond_qt, tthird_qt, tfull])
        c=1.0-np.max((r/row.start_total_impressions))
        tune_out = 100.0*(c)                  
        method=8 # the worst method    
    
  else:       
    # the best data 
    if (row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt)/(1.0*row.start_total_impressions)))
        method=1
    elif (row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt+row.second_qt)/(1.0*row.start_total_impressions)))
        method=2
    elif (row.first_qt+row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt+row.second_qt+row.first_qt)/(1.0*row.start_total_impressions)))
        method=3   
    else:    
        r = np.array([ row.first_qt, row.second_qt, row.third_qt, row.full])
        c=1.0-np.max((r/(1.0*row.start_total_impressions)))
        tune_out = 100.0*(c)                  
        method=4
  if np.isnan(tune_out) == True:
    tune_out=-1

  tune_out=int(100*tune_out)/100.
  return pd.Series({'method': method, 'tune_out': tune_out, 'podNum':row.podNum, \
                    'spot':row.spot, 'showType':row.showType, 'ad_title':row.ad_title, \
                    'series_title':row.series_title, 'program_airing_id':row.program_airing_id, 'duration':row.duration, 'start_total_impressions':row.start_total_impressions, 'total_impressions':row.total_impressions })

df=data.apply(get_tune_out, axis=1)

#
#
# cool ! now plot
df=df.sort(['spot'], ascending=[1])

# for top chef, the predictions
#tcpre=[96.42, 96.80, 95.48, np.nan, np.nan, 95.44, 88.10, np.nan, np.nan]
#
#cdf=df[df.podNum==3]
#viewability=np.array(cdf['tune_out'])
#impressions=np.array(cdf['total_impressions'])
#temp=impressions
##impressions[3]=np.mean(temp[2:4])
##impressions[4]=np.mean(temp[3:5])
#impressions[impressions<200]=np.nan
#impressions=interpolate_nans(impressions)
#nimp=impressions/np.max(impressions)
#N=3
#nimp2=np.convolve(nimp, np.ones((N,))/N, mode='valid')
#nimp2=[nimp[0]]+list(nimp2)+[nimp[-1]]
#spots=range(1,len(viewability)+1)
#
#fig=plt.figure()
#ax=fig.add_subplot(111)
#ax.scatter(spots, tcpre, c='r', s=30,edgecolors=None)
#ax.scatter(spots,viewability, c='b', s=30, edgecolors=None)
#ax.plot(spots,100.0* np.array(nimp2), c='g', lw=2, ls='--', alpha=0.5)
#plt.legend(['Audience', 'Predicted', 'Actual' ], loc='lower right')
#ax.set_ylim(60, 100)
#ax.set_xlabel('Pod Position')
#ax.set_ylabel('Viewability Score')


#spots=range(1,10)
cdf=df[df.podNum==4]

viewability=np.array(cdf['tune_out'])
impressions=np.array(cdf['total_impressions'])
print(cdf['total_impressions'])
if impressions[-1] < 0.5*impressions[0]:
    impressions[-1]=1.01*impressions[0]

#impressions[impressions<10]=impressions[impressions<10]*100
impressions[impressions<200]=np.nan
impressions[impressions<2000]=impressions[impressions<2000]*10
impressions[impressions<10000]=impressions[impressions<10000]*3
impressions[impressions<20000]=impressions[impressions<20000]*1.5
impressions[impressions<20000]=impressions[impressions<20000]*1.5

#impressions[impressions<1500]=impressions[impressions<1500]*2
#impressions[impressions<1500]=impressions[impressions<1500]*2
#impressions[impressions<1500]=impressions[impressions<1500]*2

impressions=interpolate_nans(impressions)
print(impressions)
temp=impressions
#impressions[-3]=np.mean(temp[-4:-2])
#impressions[-4]=np.mean(temp[-5:-3])
nimp=impressions/np.max(impressions)
N=3
nimp2=np.convolve(nimp, np.ones((N,))/N, mode='valid')
nimp2=[nimp[0]]+list(nimp2)+[nimp[-1]]

fig=plt.figure()
ax=fig.add_subplot(111)
ax.scatter(range(1, len(viewability)+1),viewability, c='b', s=30, edgecolors=None)
ax.plot(range(1, len(viewability)+1),100.0* np.array(nimp2), c='g', lw=2, ls='--', alpha=0.5)
plt.legend(['Audience',  'Actual' ], loc='lower right')
ax.set_ylim(60, 100)
ax.set_xlabel('Pod Position')
ax.set_ylabel('Viewability Score')

plt.show()
