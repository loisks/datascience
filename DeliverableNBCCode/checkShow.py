# checkShow.py
#
# check the show the Colony and how ads performed on this show
# get the best performers
#
# LKS, February 2017
#
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
from scipy.stats import ttest_ind


data=pd.read_csv('superstorelonger.csv', sep=';', header=0)


def get_tune_out(row):
  #
  # input
  # row = row of pandas dataframe
  #
  # output
  # tuneout rate row and other things
  #
  if row.start == 0:
    # redistribute the wealth
    tstart = (.7*row.first_qt)+(.2*row.second_qt)+(.01*row.third_qt)+\
             (.01*row.full)
    tfirst_qt=row.first_qt- .7*row.first_qt
    tsecond_qt=row.second_qt-.2*row.second_qt
    tthird_qt=row.third_qt- .01*row.third_qt
    tfull =row.full- .01*row.full
    
    # calculate tune out rate from this 
    if (tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt)/(1.0*row.start_total_impressions)))
        method=5
    elif (tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt+tsecond_qt)/(1.0*row.start_total_impressions)))
        method=6
    elif (tfirst_qt+tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt+tsecond_qt+tfirst_qt)/(1.0*row.start_total_impressions)))
        method=7
    else:
        r = np.array([ tfirst_qt, tsecond_qt, tthird_qt, tfull])
        c=1.0-np.max((r/row.start_total_impressions))
        tune_out = 100.0*(c)                  
        method=8 # the worst method    
    
  else:       
    # the best data 
    if (row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt)/(1.0*row.start_total_impressions)))
        method=1
    elif (row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt+row.second_qt)/(1.0*row.start_total_impressions)))
        method=2
    elif (row.first_qt+row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt+row.second_qt+row.first_qt)/(1.0*row.start_total_impressions)))
        method=3   
    else:    
        r = np.array([ row.first_qt, row.second_qt, row.third_qt, row.full])
        c=1.0-np.max((r/(1.0*row.start_total_impressions)))
        tune_out = 100.0*(c)                  
        method=4
  if np.isnan(tune_out) == True:
    tune_out=-1

  tune_out=int(100*tune_out)/100.
  return pd.Series({'method': method, 'tune_out': tune_out, 'podNum':row.podNum, \
                    'spot':row.spot, 'showType':row.showType, 'ad_title':row.ad_title, \
                    'series_title':row.series_title, 'program_airing_id':row.program_airing_id, 'duration':row.duration })

df=data.apply(get_tune_out, axis=1)

df = df[(df.method != 4) & (df.method != 8)]
# need to separate by program airing id
uniquePrograms=df.program_airing_id.unique()

dur15=df[df.duration==15]
dur30=df[df.duration==30]

print('15 seconds: ')
print(dur15['tune_out'].mean())
print(dur15['tune_out'].median())
print(dur15['tune_out'].std())
print('30 seconds: ')
print(dur30['tune_out'].mean())
print(dur30['tune_out'].median())
print(dur30['tune_out'].std())


# having issues with pod letter and getting this to load 
#for iuni in uniquePrograms:
#    dftemp = df[df.program_airing_id == iuni]
#
#    # there are six pods 
#    for iPod in range(1, 7): 
#        #plot the tune out rate across pod
#        dftemp2=dftemp[dftemp.podNum == iPod]
#        os.chdir('ColonyTest')
#        fig=plt.figure()
#        ax=fig.add_subplot(111)
#        ax.scatter(dftemp2['spot'], dftemp2['tune_out'], marker='x')
#        ax.set_ylabel('Viewability Score')
#        ax.set_xlabel('Pod Position')
#        plt.savefig('program='+str(iuni)+'_pod='+str(iPod)+'.png')
#        plt.close()
#        os.chdir('..') 
