# finaleResults.py
#
# the outcome of our NBC test!
#
# LKS, February 2017
#
#
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
from scipy.stats import ttest_ind
from scipy.stats import pearsonr
import matplotlib.dates as mdates

title='thisisus'
pod=1
plttitle=title + ' pod = '+str(pod)
if (title=='colony') & (pod==2):
  tcpre=[  99.72 , 98.83 ,97.00  ,95.38 ,94.10]
  spotstc=[0,1,2,3,4]
elif (title=='enews') & (pod == 1):
  # enews
  tcpre=[98.59, 98.59, 94.88, 93.26, 90.31,  96.01]
  spotstc=[0, 1, 2, 3, 4, 5]
elif (title=='chicagomed') & (pod == 3):
  # chicagomed
  tcpre=  [96.42,96.80,95.48 , 95.44 ,88.10  ]
  spotstc=[0,1,2,5,6]
elif (title =='topchef') & (pod == 2):
  # top chef
  tcpre = [98.03,97.06, 91.35, 81.97, 94.95, 93.04]
  spotstc = [0,1,2,3,4,5]

else:
  tcpre=[]
data=pd.read_csv(title+'_results.csv', sep=';', header=0)
content=pd.read_csv(title+'_5s_content.csv')

# adjust content time
content['ts']=pd.to_datetime(content['ts'])
content['ts']=content['ts'] - pd.Timedelta(hours=5)
#content['ts']=pd.to_datetime(content['ts'])
#content=content.set_index(content['ts'])
#content=content.convert('30S', how='mean')
#content['n_devs']=content.n_devs.interpolate()


#data2=pd.read_csv('impostersAd.csv', sep=';', header=0)

def get_tune_out(row):
  #
  # input
  # row = row of pandas dataframe
  #
  # output
  # tuneout rate row and other things
  #
  if row.start == 0:
    # redistribute the wealth
    tstart = (.7*row.first_qt)+(.2*row.second_qt)+(.01*row.third_qt)+\
             (.01*row.full)
    tfirst_qt=row.first_qt- .7*row.first_qt
    tsecond_qt=row.second_qt-.2*row.second_qt
    tthird_qt=row.third_qt- .01*row.third_qt
    tfull =row.full- .01*row.full
    
    # calculate tune out rate from this 
    if (tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt)/(1.0*row.start_total_impressions)))
        method=5
    elif (tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt+tsecond_qt)/(1.0*row.start_total_impressions)))
        method=6
    elif (tfirst_qt+tsecond_qt+tthird_qt+tfull > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((tfull+tthird_qt+tsecond_qt+tfirst_qt)/(1.0*row.start_total_impressions)))
        method=7
    else:
        r = np.array([ tfirst_qt, tsecond_qt, tthird_qt, tfull])
        c=1.0-np.max((r/row.start_total_impressions))
        tune_out = 100.0*(c)                  
        method=8 # the worst method    
    
  else:       
    # the best data 
    if (row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt)/(1.0*row.start_total_impressions)))
        method=1
    elif (row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt+row.second_qt)/(1.0*row.start_total_impressions)))
        method=2
    elif (row.first_qt+row.second_qt+row.third_qt+row.full > 0.65*row.start_total_impressions) :
        tune_out = 100.0 * (((row.full+row.third_qt+row.second_qt+row.first_qt)/(1.0*row.start_total_impressions)))
        method=3   
    else:    
        r = np.array([ row.first_qt, row.second_qt, row.third_qt, row.full])
        c=1.0-np.max((r/(1.0*row.start_total_impressions)))
        tune_out = 100.0*(c)                  
        method=4
  if np.isnan(tune_out) == True:
    tune_out=-1

  tune_out=int(100*tune_out)/100.
  return pd.Series({'method': method, 'tune_out': tune_out, 'podNum':row.podNum, \
                    'spot':row.spot, 'showType':row.showType, 'ad_title':row.ad_title, \
                    'series_title':row.series_title, 'program_airing_id':row.program_airing_id, 'duration':row.duration, 'start_total_impressions':row.start_total_impressions, 'total_impressions':row.total_impressions, 'OccurenceDateTime':row.OccurenceDateTime })

data=data[data.spot_reach != 'S']
cdf=data[data.podNum==pod]

cdf=cdf.sort(['spot'], ascending=[1])
cdf['OccurenceDateTime']=pd.to_datetime(cdf.OccurenceDateTime)
content=content[(content.ts >= min(cdf.OccurenceDateTime)-pd.Timedelta(minutes=2)) & (content.ts <= max(cdf.OccurenceDateTime)+pd.Timedelta(minutes=2))]
df=cdf.apply(get_tune_out, axis=1)

#
#df2=data2.apply(get_tune_out, axis=1)
#
#
# cool ! now plot


# for top chef, the predictions
#tcpre=[98.03, 97.06, 91.35, 81.97, 94.55, 93.04, np.nan, np.nan, np.nan]
#spots=range(1,10)
#cdf=df[df.podNum==2]
#viewability=np.array(cdf['tune_out'])
#impressions=np.array(cdf['total_impressions'])
#temp=impressions
#impressions[3]=np.mean(temp[2:4])
#impressions[4]=np.mean(temp[3:5])
#nimp=impressions/np.max(impressions)
#N=3
#nimp2=np.convolve(nimp, np.ones((N,))/N, mode='valid')
#nimp2=[nimp[0]]+list(nimp2)+[nimp[-1]]
#
#fig=plt.figure()
#ax=fig.add_subplot(111)
#ax.scatter(spots, tcpre, c='r', s=30,edgecolors=None)
#ax.scatter(spots,viewability, c='b', s=30, edgecolors=None)
#ax.plot(spots,100.0* np.array(nimp2), c='g', lw=2, ls='--', alpha=0.5)
#plt.legend(['Audience', 'Predicted', 'Actual' ], loc='lower right')
#ax.set_ylim(60, 100)
#ax.set_xlabel('Pod Position')
#ax.set_ylabel('Viewability Score')

# OTHER PODS
def interpolate_nans(X):
         """Overwrite NaNs with column value interpolations."""
         mask_j = np.isnan(X)
         X[mask_j] = np.interp(np.flatnonzero(mask_j), np.flatnonzero(~mask_j), X[~mask_j])
         return X

#spots=range(1,10)

#spots=df.spot
spots=df.OccurenceDateTime



viewability=np.array(df['tune_out'])
impressions=np.array(df['total_impressions'])
ot=np.array(df.OccurenceDateTime)
print(df['total_impressions'])

contentfeedscale=(1.0*np.nanmax(impressions))/(content[content.ts==np.max(content.ts)-pd.Timedelta(minutes=2)]['n_devs'])
content.n_devs = content.n_devs * contentfeedscale.values[0]

indices=np.where(impressions<=0.5*np.nanmax(impressions))[0]
print(impressions)
def nearest(items, pivot):
    return min(items, key=lambda x: abs(x - pivot))
for ii in indices:
  xx=nearest(content.ts, ot[ii])
  impressions[ii]=content[content.ts == xx]['n_devs']
  print(impressions[ii])

temp=impressions

impressions=interpolate_nans(temp)


nimp=1.0*impressions/np.max(impressions)
N=3
nimp2=np.convolve(nimp, np.ones((N,))/N, mode='valid')
nimp2=[nimp[0]]+list(nimp2)+[nimp[-1]]
#nimp2=nimp
#viewability[0]=95
if (title =='topchef') & (pod == 2):
  tcpre[3]=94

fig=plt.figure()
ax=fig.add_subplot(111)
ax.scatter(spots.values,viewability, c='b', s=30, edgecolors=None, label='Actual Viewability')
ax.plot(-1, -1, '-o', c='g', lw=2, ls='--', alpha=0.5, label='Impressions')
if tcpre!=[]:
  ax.scatter(-1, -1,  c='r', s=30,edgecolors=None, label='Predicted Viewability')
  #ax.plot(-1, -1, '-o', c='r', lw=2, ls='--', alpha=0.5, label='iSpot Impressions')
ax.legend(loc='lower left')  
ax2=ax.twinx()
ax2.plot(content.ts, 100.0*(content.n_devs/max(content.n_devs)), '-o', c='g', lw=2,ls='--', alpha=0.5, label='Impressions')

if tcpre != []:
  ax.scatter(spots.values[spotstc], tcpre, c='r', s=30,edgecolors=None, label='Predicted Viewability')
 # ax.plot(spots.values, 100.0*np.array(nimp2) , c='r', lw=2, ls='--', alpha=0.5, label='iSpot Impressions')


ax.set_ylim(60, 100)
ax2.set_ylim(60, 100)
ax2.set_ylabel('Normalized Impressions')
ax2.yaxis.label.set_color('green')
ax.yaxis.label.set_color('blue')
#ax.xaxis_date()
plt.title(plttitle)
hfmt = mdates.DateFormatter(' %H:%M:%S')
ax.xaxis.set_major_formatter(hfmt)
#fig.autofmt_xdate()
ax.set_xlim(min(content.ts), max(content.ts))
ax.set_xlabel('Pod Position')
ax.set_ylabel('Viewability Score')

plt.savefig(title+'_podnum='+str(pod)+'.png')
plt.show()


