# FacebookProject.py
#
# make a huge SQL query
#
# March 2017, LKS
#
import numpy as np
import datetime
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
from matplotlib.pyplot import cm 

#listOfUnique=[1029151,1050439,1050888,1243737,1245576,1321593,1321884,1330685,1331338,1331422,1332915,1361096,1361420,1364054,1404508,1404863,1404901,1407745,1412542,1426594,1431909,1432353,1432894,1451319,1453746,1460187,1464718,1468380,1472081,1472192,1482588,1484918]
listOfUnique=[1243737]

info=pd.read_csv('adid_adtitle_duration.csv', delimiter=';', header=0)
advertisers=np.array(pd.read_csv('advertiserList.csv', delimiter=';', header=0)['advertiser_name'])
titles=np.array(info['ad_title'])
durations=np.array(info['duration'])

Results=[]
Time=[]

fileW= open('output.txt', 'wb')
for item in range(len(listOfUnique)):
    os.chdir('lois_ad_values_with_day_part_airings_detail_segment_pod_position')
    file= pd.read_csv('garren_lois_ad_values_with_daypart_segment_pod_position.tsv.'+str(listOfUnique[item])+'.split',sep='\t', header=None)
    stop
    os.chdir('..')
    file['diff']= pd.to_datetime(file[9])-pd.to_datetime(file[6])
    #
    # now get a mean and std of percent viewed
    fileW.write('Adid: '+str(listOfUnique[item]))
    fileW.write('\n')
    fileW.write('Ad Title: '+ titles[item])
    fileW.write('\n')
    fileW.write('Duration: '+ str(durations[item]))
    fileW.write('\n')
    fileW.write('Overall mean: '+ str(file[2].mean()))
    fileW.write('\n')
    fileW.write('Overall std: '+str(file[2].std()))
    fileW.write('\n')
    #
    # get mean by series
    fileW.write('By Series')
    fileW.write('\n')
    file[2]=100.0*file[2]/file[2].max()
    c=file.groupby([4])[2].agg(['mean', 'count', 'std'])
    dd=c[c['count'] > 100]
    dde=dd.sort_values(['mean'], ascending=False)

    # print five best and worst
    fileW.write('Five Best: '+str(dde.head(n=5)))
    fileW.write('\n')
    fileW.write('Five Worst: '+str( dde.tail(n=5))   )
    fileW.write('\n')

    fileW.write('By DMA')
    fileW.write('\n')
    c=file.groupby([3])[2].agg(['mean', 'count', 'std'])
    dd=c[c['count'] > 100]
    dde=dd.sort_values(['mean'], ascending=False)

    # print five best and worst
    fileW.write('Five Best: '+str(dde.head(n=5)))
    fileW.write('\n')
    fileW.write('Five Worst: '+str( dde.tail(n=5))   )
    fileW.write('\n')
    fileW.write('\n')

    # plot overall
    tdiff=np.array(file['diff']).astype(float)/(1.0e9) # because it's in nanoseconds
    #tdiff[tdiff >(durations[item]-(durations[item]*.2))] = durations[item]
    dataD=np.zeros(durations[item])
    for view in range(0,durations[item]):
        dataD[view]=len(np.where(tdiff >= view+1)[0])
    if durations[item] < 20:
        dataD=dataD[0:11]
    elif (durations[item] > 20) & ( durations[item] < 45):
        dataD=dataD[0:24]
    else:
        dataD=dataD
    #if len(np.where(dataD < .8*np.max(dataD))[0]) > 0:
    #    dataD=dataD[:np.where(dataD < .8*np.max(dataD))[0][0]]
    
    fig=plt.figure()
    ax=fig.add_subplot(111)
    ax.plot(np.linspace(1, durations[item]+1, len(dataD)), dataD, lw=2, color='b')
    os.chdir('DevicesWatching')
    ax.set_ylabel('Devices Watching')
    ax.set_xlabel('Seconds') 
    ax.grid(True)
    plt.savefig(str(listOfUnique[item])+'_devicesWatching.png')
    os.chdir('..')
    meanD=dataD
    Results.append(dataD)
    Time.append(np.linspace(1, durations[item]+1, len(dataD)))

    #
    # OK now do it by individual ad and occurrences
    uniOccur=file[10].unique()
    os.chdir('UniqueOccurrences')
    fig2=plt.figure()
    ax2=fig2.add_subplot(111)
    ax2.set_ylabel('Devices Watching')
    ax2.set_xlabel('Seconds')
    plt.title('By Unique Occurrences For '+ titles[item])
    ax2.grid(True)
    ax2.set_ylim(0.8,1)
    for iO in range(len(uniOccur)):
        temp=file[file[10]==uniOccur[iO]]
        if len(temp) > 100:
            tdiff=np.array(temp['diff']).astype(float)/(1.0e9) # because it's in nanoseconds
            
            dataD=np.zeros(durations[item])
            for view in range(0,durations[item]):
                dataD[view]=len(np.where(tdiff >= view+1)[0])
            if durations[item] < 20:
                dataD=dataD[0:11]
            elif (durations[item] > 20) & ( durations[item] < 45):
                dataD=dataD[0:24]
            else:
                dataD=dataD
            
            dataD=dataD/(1.0*np.max(dataD))
            ax2.plot(np.linspace(1, durations[item]+1, len(dataD)), dataD, lw=2, color='k', alpha=0.1)

    ax2.plot(np.linspace(1, durations[item]+1, len(meanD)), meanD/(1.0*np.max(meanD)), lw=2, color='r')
    try:
        plt.savefig("u"+str(listOfUnique[item])+'_devicesWatchingbyOccurrence.png')
    except:
        print('whatever')
    os.chdir('..')
    
        
    # now do it by daypart 
    daypartOccur=np.sort(file[11].unique())
    os.chdir('Daypart')
    cmap=cm.jet
    #colorMap=iter(cm.jet(np.linspace(0,1,len(daypartOccur))))
    fig3=plt.figure()
    ax3=fig3.add_subplot(111)
    ax3.set_ylabel('Devices Watching')
    ax3.set_xlabel('Seconds')
    plt.title('By Daypart For '+titles[item])
    ax3.grid(True)
    ax3.set_ylim(0.8,1)
    for iO in range(len(daypartOccur)):
        temp=file[file[11]==daypartOccur[iO]]
        tdiff=np.array(temp['diff']).astype(float)/(1.0e9) # because it's in nanoseconds
   
        dataD=np.zeros(durations[item])
        for view in range(0,durations[item]):
            dataD[view]=len(np.where(tdiff >= view+1)[0])
        if durations[item] < 20:
            dataD=dataD[0:11]
        elif (durations[item] > 20) & ( durations[item] < 45):
            dataD=dataD[0:24]
        else:
            dataD=dataD

        dataD=dataD/(1.0*np.max(dataD))
        ax3.plot(np.linspace(1, durations[item]+1, len(dataD)), dataD, lw=2, color=cmap(iO / float(len(daypartOccur))))

    #ax3.plot(np.linspace(1, durations[item]+1, len(meanD)), meanD/(1.0*np.max(meanD)), lw=2, color='r')
    plt.legend(['day part = '+str(i) for i in daypartOccur], loc='lower left')
    try:
        plt.savefig(str(listOfUnique[item])+'_devicesWatchingbyDaypart.png')
    except:
        print('whatever')
    os.chdir('..')
    
fileW.close() 

# find a way to deal with the zeros in the durations
Adverts=['Tide', 'Secret', 'Pantene']
colors=['r', 'b', 'g']

fig=plt.figure()
ax=fig.add_subplot(111)
ax.set_xlim(1,60)
ax.set_ylim(0.8, 1)
ax.set_ylabel('Normalized Devices Watching')
ax.set_xlabel('Seconds')
plt.title('All Advertisers Mean by Ad') 
for iAd in range(len(Adverts)):
    ii=np.where(advertisers==Adverts[iAd])[0]
    iResult=np.array(Results)[ii]
    iTime=np.array(Time)[ii]
    ax.grid(True)
    for jj in range(len(iResult)):
        tr=iResult[jj]
        tr=np.array(tr)/np.max(tr)
        if (len(tr[tr<0.8]) > 0) & (len(tr) < 40):
            print('nope!')
        else:
            ax.plot(iTime[jj], tr, alpha=0.3, lw=2, color=colors[iAd])
l1,=ax.plot(iTime[0], np.zeros(len(iTime[0])), lw=2, alpha=0.3, color=colors[0])
l2,=ax.plot(iTime[0], np.zeros(len(iTime[0])), lw=2, alpha=0.3, color=colors[1])
l3,=ax.plot(iTime[0], np.zeros(len(iTime[0])), lw=2, alpha=0.3, color=colors[2])
plt.legend([l1,l2,l3],Adverts, loc='lower left')
os.chdir('DevicesWatching')
plt.savefig("u"+'AllAdvertisers.png')
os.chdir('..')
        
        
