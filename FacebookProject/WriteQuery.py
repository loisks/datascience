# FacebookProject.py
#
# make a huge SQL query
#
# March 2017, LKS
#
import numpy as np
import datetime

startDate='20170101'
endDate='20170201'
sDT=datetime.datetime.strptime(startDate, '%Y%m%d')
eDT=datetime.datetime.strptime(endDate, '%Y%m%d')

str=" "

while sDT < eDT:
    temp="SELECT device_uuid, ad_id, percent_viewed,dma, series.title FROM audience_raw_tables.ap_results_production_{0}_output_inferencing JOIN ispot_db.adoccurence ado on occurrence_id = ado.occurenceid JOIN ispot_db.schedules sched on ado.scheduleid = sched.scheduleid JOIN ispot_db.programs series on sched.program = series.tmsid WHERE replaced_by is NULL AND scope_platform = 'national' AND ad_id = 1029151 AND occurrence_id IS NOT NULL AND percent_viewed is NOT NULL AND dma is not NULL".format(datetime.datetime.strftime(sDT, '%Y%m%d'))
    str+=temp
    str+=' union '
    sDT+=datetime.timedelta(days=1)    

str=str[:-6]
str+=';'
