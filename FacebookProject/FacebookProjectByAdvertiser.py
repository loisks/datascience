# FacebookProject.py
#
# make a huge SQL query
#
# March 2017, LKS
#
import numpy as np
import datetime
import matplotlib.pyplot as plt
import pickle
import os
import pandas as pd
from matplotlib.pyplot import cm 

listOfUnique=[1029151,1050439,1050888,1243737,1245576,1321593,1321884,1330685,1331338,1331422,1332915,1361096,1361420,1364054,1404508,1404863,1404901,1407745,1412542,1426594,1431909,1432353,1432894,1451319,1453746,1460187,1464718,1468380,1472081,1472192,1482588,1484918]

info=pd.read_csv('adid_adtitle_duration.csv', delimiter=';', header=0)
advertisers=np.array(pd.read_csv('advertiserList.csv', delimiter=';', header=0)['advertiser_name'])
titles=np.array(info['ad_title'])
durations=np.array(info['duration'])

Adverts=['Tide']
colors=['r', 'b', 'g']


for iAdv in range(len(Adverts)):
    listAdv=np.array(listOfUnique)[advertisers==Adverts[iAdv]]
    Results=[]
    Time=[]
    DayPart=[]
    for item in range(len(listAdv)):
        os.chdir('lois_ad_values_with_day_part')
        file= pd.read_csv('lois_ad_values_with_day_part.tsv.'+str(listAdv[item])+'.split',sep='\t', header=None)
        os.chdir('..')
        file['diff']= pd.to_datetime(file[9])-pd.to_datetime(file[6])
  

        # plot overall
        tdiff=np.array(file['diff']).astype(float)/(1.0e9) # because it's in nanosecond
        uniOccur=file[10].unique()
        for iO in range(len(uniOccur)):
           temp=file[file[10]==uniOccur[iO]]
           if len(temp) > 100:
            tdiff=np.array(temp['diff']).astype(float)/(1.0e9) # because it's in nanoseconds
            dataD=np.zeros(durations[item])
            for view in range(0,durations[item]):
                dataD[view]=len(np.where(tdiff >= view+1)[0])
            if durations[item] < 20:
                dataD=dataD[0:11]
         
  
           
            elif (durations[item] > 20) & ( durations[item] < 45):
                dataD=dataD[0:24]
                dataD=dataD/(1.0*np.max(dataD))
                Results.append(dataD)
                Time.append(np.linspace(1, durations[item]+1, len(dataD)))
                # now break into day part
                DayPart.append(np.array(temp[11])[0])
            else:
                dataD=dataD
            

        
    # now do it by daypart 
    os.chdir('ByAdvertiser')
    cmap=cm.jet
    Results=np.array(Results); Time=np.array(Time)
    DayPart=np.array(DayPart)
    daypartOccur=np.unique(DayPart) 
    fig3=plt.figure()
    ax3=fig3.add_subplot(111)
    ax3.set_ylabel('Devices Watching')
    ax3.set_xlabel('Seconds')
    plt.title('By Daypart For '+Adverts[iAdv])
    ax3.grid(True)
    ax3.set_ylim(0.8,1)    
    
    for iO in daypartOccur:
        mDP=Results[DayPart==iO]
        tDP=Time[DayPart==iO]
        ax3.plot(tDP[0],np.mean(mDP,axis=0), lw=2, color=cmap(iO / float(len(daypartOccur))))
       # for ii in range(len(mDP)):
       #     ax3.plot(tDP[ii],mDP[ii], lw=2, alpha=0.1, color=cmap(iO / float(len(daypartOccur))))

    #ax3.plot(np.linspace(1, durations[item]+1, len(meanD)), meanD/(1.0*np.max(meanD)), lw=2, color='r')
    plt.legend(['day part = '+str(i) for i in daypartOccur], loc='lower left')
    plt.savefig('AllTide30secDaypart.png')
    os.chdir('..')
    

# find a way to deal with the zeros in the durations
